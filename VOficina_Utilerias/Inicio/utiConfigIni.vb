﻿Imports System.Xml
Public Class utiConfigIni
    Private Sub ConfigIni_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btn_Ok_Click(sender As Object, e As EventArgs) Handles btn_Ok.Click
        Try
            Dim xmlDoc As XmlDocument
            Dim sXml As String = "<archivo_inicio>" & vbNewLine _
                                & vbTab & "<id_empresa>#id_empresa#</id_empresa>" & vbNewLine _
                                & vbTab & "<sql>" & vbNewLine _
                                    & vbTab & "<server>#servidor#</server>" & vbNewLine _
                                    & vbTab & "<basedatos>#basedatos#</basedatos>" & vbNewLine _
                                    & vbTab & "<usuario>#user#</usuario>" & vbNewLine _
                                    & vbTab & "<contrasena>#pass#</contrasena>" & vbNewLine _
                                & vbTab & "</sql>" & vbNewLine _
                                & "</archivo_inicio>"

            sXml = sXml.Replace("#id_empresa#", "")

            sXml = sXml.Replace("#servidor#", txt1_ConfigServer.Text)
            sXml = sXml.Replace("#basedatos#", txt1_ConfigBaseDatos.Text)
            sXml = sXml.Replace("#user#", txt1_ConfigUser.Text)
            sXml = sXml.Replace("#pass#", txt1_ConfigPass.Text)

            RichTextBox1.Text = sXml

            xmlDoc = New XmlDocument()

            If txt1_Llave.Text <> "" Then
                If RichTextBox1.Text <> "" Then
                    xmlDoc.LoadXml(RichTextBox1.Text)
                    RichTextBox1.Text = CryptoZ.Encrypt(xmlDoc.OuterXml, txt1_Llave.Text)
                    If SFD1.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                        If Not IO.File.Exists(SFD1.FileName) Then
                            RichTextBox1.SaveFile(SFD1.FileName)
                        End If
                        RichTextBox1.SaveFile(SFD1.FileName)
                    End If
                Else
                    System.Windows.Forms.MessageBox.Show("No hay texto para convertir en XML", Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                End If
            Else
                System.Windows.Forms.MessageBox.Show("Debe Proporcionar la llave para encriptar o desencriptar.", Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("Ocurrio un error: " & ex.Message.ToString(), Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class