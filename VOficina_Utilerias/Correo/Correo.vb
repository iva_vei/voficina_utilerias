﻿Imports System.Windows.Forms

Public Class Correo
    Dim DirectorioTemp As String = "C:\Windows\Temp\"
    Dim XmlRuta As String = "C:\Voficina\DLLs"
    Public Sub New()
        Dim Msj As String = ""
        Dim Minor As String
        Dim MinorRevision As String
        Dim Major As String
        Dim MajorRevision As String

        Try
            Globales.oAmbientes.oFormulario = New Globales.clsFormularios

            Globales.oAmbientes.Sistema = Application.ProductName
            Globales.oAmbientes.cnx_Control = New Globales.clsConexion
            Globales.oAmbientes.cnx_Conexiones = New Dictionary(Of String, Globales.clsConexion)()

            Minor = Format(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build, "000")
            MinorRevision = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.MinorRevision
            Major = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major
            MajorRevision = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString

            Globales.oAmbientes.SW_Version = False
            Globales.oAmbientes.Version = Major & "." & MajorRevision & "." & Minor

            If Not Globales.oAmbientes.CargarXmlCnx(XmlRuta, "Inicio.ini", Msj) Then
                Call ConfigIni()
            End If
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub ConfigIni()
        Dim oFrm As utiConfigIni
        Try
            oFrm = New utiConfigIni
            oFrm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(Globales.oAmbientes.oFormulario.Msj_Error & vbNewLine & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
    Public Function EnviaCorreoPedido(ByVal conexion As String _
                         , ByVal Id_Empresa As String _
                         , ByVal Id_Sucursal As String _
                         , ByVal var_Asunto As String _
                         , ByVal var_Mensaje As String _
                         , ByVal var_Proveedor As String _
                         , ByVal var_Pedido As String _
                         , ByVal Formato As String _
                         , ByVal SQLPost As String _
                         , ByRef Mensaje As String) As Boolean
        Dim FormatoOC As Object
        Dim oPedidos As pedidos
        Dim printTool As DevExpress.XtraReports.UI.ReportPrintTool

        Dim oDatos As Datos_VOficina = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim dtProv As DataTable = Nothing
        Dim dtPed As DataTable = Nothing
        Dim dtPedDet As DataTable = Nothing

        Dim Sw_Regresa As Boolean = False
        Dim Smtp_Server As New System.Net.Mail.SmtpClient
        Dim e_mail As New System.Net.Mail.MailMessage()

        Dim var_MailSsl As Boolean = True
        Dim var_MailUser As String = ""
        Dim var_MailPass As String = ""
        Dim var_MailHost As String = ""
        Dim var_MailPort As String = ""
        Dim var_MailFrom As String = ""
        Dim var_MailDestinos As String = ""
        Dim var_Archivo As String = ""
        Dim sSQL As String = ""
        Try
            sSQL = ""
            sSQL = sSQL & vbNewLine & "SELECT TOP 1 * "
            sSQL = sSQL & vbNewLine & "FROM datoscorreo D(NOLOCK)"
            sSQL = sSQL & vbNewLine & "WHERE D.id_empresa  = '" & Id_Empresa & "'"
            sSQL = sSQL & vbNewLine & "AND   D.id_sucursal = '" & Id_Sucursal & "'"

            oDatos = New Datos_VOficina
            If oDatos.SQLQuery(conexion, sSQL, dtDatos, Mensaje) Then
                If dtDatos.Rows.Count > 0 Then

                    var_MailFrom = dtDatos.Rows(0).Item("Usuario")
                    var_MailUser = dtDatos.Rows(0).Item("Usuario")
                    var_MailPass = dtDatos.Rows(0).Item("Contrasena")
                    var_MailHost = dtDatos.Rows(0).Item("Host")
                    var_MailPort = dtDatos.Rows(0).Item("Puerto")
                    var_MailSsl = dtDatos.Rows(0).Item("ssl")

                    If var_MailUser <> "" And var_MailPass <> "" _
                    And var_MailPort <> "" And var_MailHost <> "" _
                    And var_MailFrom <> "" Then

                        sSQL = ""
                        sSQL = sSQL & vbNewLine & "SELECT * "
                        sSQL = sSQL & vbNewLine & "FROM proveedores D(NOLOCK)"
                        sSQL = sSQL & vbNewLine & "WHERE D.proveedor  = " & var_Proveedor & ""

                        oDatos = New Datos_VOficina
                        If oDatos.SQLQuery(conexion, sSQL, dtProv, Mensaje) Then
                            var_MailDestinos = dtProv.Rows(0).Item("email")
                            sSQL = ""
                            sSQL = sSQL & vbNewLine & "SELECT TOP 1 P.* "
                            sSQL = sSQL & vbNewLine & "       ,c.nombre   AS nomcomp "
                            sSQL = sSQL & vbNewLine & "       ,c.nombre   AS comprador_nombre "
                            sSQL = sSQL & vbNewLine & "       ,c.clave    AS comprador_clave "
                            sSQL = sSQL & vbNewLine & "       ,c.correo   AS comprador_correo "
                            sSQL = sSQL & vbNewLine & "       ,c.contacto AS comprador_contacto"
                            sSQL = sSQL & vbNewLine & "       ,LTRIM(RTRIM(s.direccion)) + ' ' + LTRIM(RTRIM(s.colonia)) AS suc1_direccion "
                            sSQL = sSQL & vbNewLine & "       ,LTRIM(RTRIM(s.ciudad))    + ' ' + LTRIM(RTRIM(s.estado)) + s.cpostal  AS suc1_ciudad "
                            sSQL = sSQL & vbNewLine & "       ,LTRIM(RTRIM(s2.direccion)) + ' ' + LTRIM(RTRIM(s2.colonia)) AS suc_entrega_direccion "
                            sSQL = sSQL & vbNewLine & "       ,LTRIM(RTRIM(s2.ciudad))    + ' ' + LTRIM(RTRIM(s2.estado)) + s2.cpostal  AS suc_entrega_ciudad "
                            sSQL = sSQL & vbNewLine & "       ,TitEmpresa		=	ISNULL(LTRIM(RTRIM(E.empresa))	,'')"
                            ''sSQL = sSQL & vbNewLine & "       ,TitRFC			=	ISNULL(LTRIM(RTRIM(E.rfc))		,'')"
                            sSQL = sSQL & vbNewLine & "       ,TitDireccion	=	ISNULL(LTRIM(RTRIM(E.direccion))"
                            sSQL = sSQL & vbNewLine & " + '|' +	LTRIM(RTRIM(E.colonia))	+	' CP:' + CAST(E.cpostal	AS VARCHAR)"
                            sSQL = sSQL & vbNewLine & " + '|' +	LTRIM(RTRIM(E.ciudad))	+	',' + LTRIM(RTRIM(E.estado))	+	',' + LTRIM(RTRIM(E.pais))"
                            sSQL = sSQL & vbNewLine & " + '|' +	'RFC: ' + LTRIM(RTRIM(E.rfc)),'')"
                            sSQL = sSQL & vbNewLine & " From pedidos        P(NOLOCK)"
                            sSQL = sSQL & vbNewLine & "Join empresas      E(NOLOCK)"
                            sSQL = sSQL & vbNewLine & "On      P.id_empresa	=	E.id_empresa"
                            sSQL = sSQL & vbNewLine & "Join pedidosdet      D(NOLOCK)"
                            sSQL = sSQL & vbNewLine & "On      D.id_empresa	=	P.id_empresa"
                            sSQL = sSQL & vbNewLine & "And		D.pedido		=	P.pedido"
                            sSQL = sSQL & vbNewLine & "Join    sucursales		S(NOLOCK) "
                            sSQL = sSQL & vbNewLine & "On      D.id_empresa	= s.id_empresa "
                            sSQL = sSQL & vbNewLine & "And     D.id_sucursal1	= s.id_sucursal "
                            sSQL = sSQL & vbNewLine & "Join    sucursales		S2 (NOLOCK) "
                            sSQL = sSQL & vbNewLine & "On      P.id_empresa	= s2.id_empresa "
                            sSQL = sSQL & vbNewLine & "And     P.surte			= s2.id_sucursal "
                            sSQL = sSQL & vbNewLine & "Left Join      compradores C (NOLOCK)"
                            sSQL = sSQL & vbNewLine & "On      C.id_empresa   = P.id_empresa "
                            sSQL = sSQL & vbNewLine & "And		LTRIM(RTRIM(C.id_comprador)) = LTRIM(RTRIM(P.id_comprador))"
                            sSQL = sSQL & vbNewLine & "WHERE P.id_empresa  = '" & Id_Empresa & "'"
                            sSQL = sSQL & vbNewLine & "AND   P.pedido = '" & var_Pedido & "'"
                            sSQL = sSQL & vbNewLine & "AND   P.proveedor = '" & var_Proveedor & "'"

                            oDatos = New Datos_VOficina
                            If oDatos.SQLQuery(conexion, sSQL, dtPed, Mensaje) Then

                                sSQL = ""
                                sSQL = sSQL & vbNewLine & "SELECT D.*, "
                                sSQL = sSQL & vbNewLine & "       A.nombre As nombre, "
                                sSQL = sSQL & vbNewLine & "       A.codigo,"
                                sSQL = sSQL & vbNewLine & "       A.codigoref, "
                                sSQL = sSQL & vbNewLine & "       A.unidadvta"
                                sSQL = sSQL & vbNewLine & " FROM    pedidos AS P (NOLOCK) "
                                sSQL = sSQL & vbNewLine & " JOIN    pedidosdet AS D (NOLOCK) "
                                sSQL = sSQL & vbNewLine & " ON      D.id_empresa  = P.id_empresa"
                                sSQL = sSQL & vbNewLine & " AND     D.pedido      = P.pedido"
                                sSQL = sSQL & vbNewLine & " JOIN    articulos AS A (NOLOCK) "
                                sSQL = sSQL & vbNewLine & " ON      A.articulo = D.articulo"
                                sSQL = sSQL & vbNewLine & " WHERE P.id_empresa  = '" & Id_Empresa & "'"
                                sSQL = sSQL & vbNewLine & " AND   P.pedido = '" & var_Pedido & "'"
                                sSQL = sSQL & vbNewLine & " AND   D.pedida > 0 "
                                sSQL = sSQL & vbNewLine & " ORDER BY D.renglon  "

                                oDatos = New Datos_VOficina
                                If oDatos.SQLQuery(conexion, sSQL, dtPedDet, Mensaje) Then
                                    oPedidos = New pedidos
                                    oPedidos.Fill(dtProv, dtPed, dtPedDet)

                                    Select Case Formato.ToUpper
                                        Case "BASE"
                                            FormatoOC = New XtraRep_OC_BASE
                                            TryCast(FormatoOC, XtraRep_OC_BASE).RutaXml = XmlRuta
                                            TryCast(FormatoOC, XtraRep_OC_BASE).Logotipo = XmlRuta & "\" & oPedidos.Id_empresa & ".jpg"
                                            TryCast(FormatoOC, XtraRep_OC_BASE).Empresa = dtPed.Rows(0).Item("TitEmpresa")
                                            TryCast(FormatoOC, XtraRep_OC_BASE).Direccion = dtPed.Rows(0).Item("TitDireccion").ToString.Replace("|", vbNewLine)
                                            TryCast(FormatoOC, XtraRep_OC_BASE).odsDatosPedidos.DataSource = oPedidos
                                        Case "PAPELERA"
                                            FormatoOC = New XtraRep_OC_PAPELERA
                                            TryCast(FormatoOC, XtraRep_OC_PAPELERA).RutaXml = XmlRuta
                                            TryCast(FormatoOC, XtraRep_OC_PAPELERA).Logotipo = XmlRuta & "\" & oPedidos.Id_empresa & ".jpg"
                                            TryCast(FormatoOC, XtraRep_OC_PAPELERA).Empresa = dtPed.Rows(0).Item("TitEmpresa")
                                            TryCast(FormatoOC, XtraRep_OC_PAPELERA).Direccion = dtPed.Rows(0).Item("TitDireccion").ToString.Replace("|", vbNewLine)
                                            TryCast(FormatoOC, XtraRep_OC_PAPELERA).odsDatosPedidos.DataSource = oPedidos
                                        Case Else
                                            FormatoOC = New XtraRep_OC_BASE
                                            TryCast(FormatoOC, XtraRep_OC_BASE).RutaXml = XmlRuta
                                            TryCast(FormatoOC, XtraRep_OC_BASE).Logotipo = XmlRuta & "\" & oPedidos.Id_empresa & ".jpg"
                                            TryCast(FormatoOC, XtraRep_OC_BASE).Empresa = dtPed.Rows(0).Item("TitEmpresa")
                                            TryCast(FormatoOC, XtraRep_OC_BASE).Direccion = dtPed.Rows(0).Item("TitDireccion").ToString.Replace("|", vbNewLine)
                                            TryCast(FormatoOC, XtraRep_OC_BASE).odsDatosPedidos.DataSource = oPedidos
                                    End Select
                                    var_Archivo = "PED_" & var_Proveedor & "_" & var_Pedido
                                    If Not FormatoOC Is Nothing Then
                                        printTool = New DevExpress.XtraReports.UI.ReportPrintTool(FormatoOC)
                                        printTool.ShowPreviewDialog()
                                        If Not IO.Directory.Exists(DirectorioTemp & var_Archivo) Then
                                            IO.Directory.CreateDirectory(DirectorioTemp & var_Archivo)
                                        End If
                                        If IO.File.Exists(DirectorioTemp & var_Archivo & "\" & var_Archivo & ".pdf") Then
                                            IO.File.Delete(DirectorioTemp & var_Archivo & "\" & var_Archivo & ".pdf")
                                        End If
                                        FormatoOC.ExportToPdf(DirectorioTemp & var_Archivo & "\" & var_Archivo & ".pdf")
                                    Else
                                        MessageBox.Show("Formato no registrado.", Globales.oAmbientes.oFormulario.Tit_Aviso, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End If


                                    Smtp_Server.UseDefaultCredentials = False
                                    Smtp_Server.Credentials = New Net.NetworkCredential(var_MailUser, var_MailPass)
                                    Smtp_Server.Port = var_MailPort
                                    Smtp_Server.EnableSsl = var_MailSsl
                                    Smtp_Server.Host = var_MailHost

                                    e_mail = New System.Net.Mail.MailMessage()
                                    e_mail.From = New System.Net.Mail.MailAddress(var_MailFrom)

                                    var_MailDestinos = InputBox("Teclea los correos para los destinatarios..." & vbNewLine & "Los correos deben estar separados por ;", "Verifica el correo a enviar. V(" & Globales.oAmbientes.Version & ")", var_MailDestinos)
                                    If var_MailDestinos <> "" Then

                                        For Each dest As String In var_MailDestinos.Split(";")
                                            If dest <> "" Then
                                                e_mail.To.Add(dest)
                                            End If
                                        Next
                                        'e_mail.CC.Add(txtTo.Text)
                                        e_mail.IsBodyHtml = True
                                        e_mail.Subject = var_Asunto
                                        e_mail.Body = var_Mensaje

                                        ''If IO.File.Exists(DirectorioTemp & var_Archivo & ".zip") Then
                                        ''    IO.File.Delete(DirectorioTemp & var_Archivo & ".zip")
                                        ''End If
                                        ''System.IO.Compression.ZipFile.CreateFromDirectory(DirectorioTemp & var_Archivo, DirectorioTemp & var_Archivo & ".zip", System.IO.Compression.CompressionLevel.Optimal, False)
                                        ''Dim attachfile As New System.Net.Mail.Attachment(DirectorioTemp & var_Archivo & ".zip")

                                        Dim attachfile As New System.Net.Mail.Attachment(DirectorioTemp & var_Archivo & "\" & var_Archivo & ".pdf")
                                        e_mail.Attachments.Add(attachfile)

                                        Smtp_Server.Send(e_mail)

                                        e_mail.Dispose()
                                        Smtp_Server.Dispose()

                                        If SQLPost <> "" Then
                                            If oPedidos.Enviado = 1 And oPedidos.Estatus.ToUpper = "ACTUALIZAD" Then
                                                If oDatos.SQLQuery(conexion, SQLPost, dtProv, Mensaje) Then
                                                    Sw_Regresa = True
                                                    Mensaje = "Pedido se ha enviado a los correos proporcionados." _
                                                        & vbNewLine & "Se ha actualizado el campo 'enviado' a 2 lo que significa que ha sido enviado por correo y la fecha de impresión del Pedido(" & var_Pedido & ")."
                                                Else
                                                    Mensaje = "Error al Ejecutar Sentencia Post." + Mensaje
                                                End If
                                            Else
                                                Sw_Regresa = True
                                                Mensaje = "Pedido NUEVAMENTE enviado a los correos proporcionados."
                                            End If
                                        Else
                                            Sw_Regresa = True
                                            Mensaje = "Pedido enviado a los correos proporcionados."
                                        End If
                                    Else
                                        Mensaje = "Debes proporcionar al menos un correo para el envio."
                                    End If
                                Else
                                    Mensaje = "Error al Obtener PedidosDet." & Mensaje
                                End If
                            Else
                                Mensaje = "Error al Obtener Pedidos." & Mensaje
                            End If
                        Else
                            Mensaje = "Error al Obtener Proveedor." & Mensaje
                        End If
                    Else
                        Mensaje = "Revisa la configuración de correo algun dato esta no esta configurado"
                    End If
                Else
                    Mensaje = "Error al Obtener Datos Correo. No existe datos para configurar el correo."
                End If
            Else
                Mensaje = "Error al Obtener Datos Correo." & Mensaje
            End If
        Catch ex As Exception
            If Not ex.InnerException Is Nothing Then
                Mensaje = ex.Message & vbNewLine & ex.InnerException.Message
            Else
                Mensaje = ex.Message
            End If
        End Try
        Return Sw_Regresa
    End Function

End Class
