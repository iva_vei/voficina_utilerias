﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class utiConfigIni
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btn_Ok = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt1_ConfigPass = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt1_ConfigUser = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt1_ConfigBaseDatos = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt1_ConfigServer = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt1_Llave = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SFD1 = New System.Windows.Forms.SaveFileDialog()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.Controls.Add(Me.RichTextBox1)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.btn_Ok)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txt1_ConfigPass)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txt1_ConfigUser)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txt1_ConfigBaseDatos)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt1_ConfigServer)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txt1_Llave)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(460, 337)
        Me.Panel1.TabIndex = 0
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(384, 65)
        Me.RichTextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(59, 39)
        Me.RichTextBox1.TabIndex = 26
        Me.RichTextBox1.Text = ""
        Me.RichTextBox1.Visible = False
        '
        'Button1
        '
        Me.Button1.Image = Global.VOficina_Utilerias.My.Resources.Resources.Cancel
        Me.Button1.Location = New System.Drawing.Point(311, 289)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(42, 42)
        Me.Button1.TabIndex = 22
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btn_Ok
        '
        Me.btn_Ok.Image = Global.VOficina_Utilerias.My.Resources.Resources.ok
        Me.btn_Ok.Location = New System.Drawing.Point(91, 289)
        Me.btn_Ok.Name = "btn_Ok"
        Me.btn_Ok.Size = New System.Drawing.Size(42, 42)
        Me.btn_Ok.TabIndex = 21
        Me.btn_Ok.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Gray
        Me.Label6.Location = New System.Drawing.Point(244, 227)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(180, 24)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "PASSWORD"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_ConfigPass
        '
        Me.txt1_ConfigPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ConfigPass.Location = New System.Drawing.Point(244, 254)
        Me.txt1_ConfigPass.Name = "txt1_ConfigPass"
        Me.txt1_ConfigPass.Size = New System.Drawing.Size(180, 29)
        Me.txt1_ConfigPass.TabIndex = 19
        Me.txt1_ConfigPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Gray
        Me.Label7.Location = New System.Drawing.Point(24, 227)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(180, 24)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "USUARIO"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_ConfigUser
        '
        Me.txt1_ConfigUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ConfigUser.Location = New System.Drawing.Point(24, 254)
        Me.txt1_ConfigUser.Name = "txt1_ConfigUser"
        Me.txt1_ConfigUser.Size = New System.Drawing.Size(180, 29)
        Me.txt1_ConfigUser.TabIndex = 17
        Me.txt1_ConfigUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Gray
        Me.Label5.Location = New System.Drawing.Point(244, 154)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(180, 24)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "BASE DATOS"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_ConfigBaseDatos
        '
        Me.txt1_ConfigBaseDatos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ConfigBaseDatos.Location = New System.Drawing.Point(244, 181)
        Me.txt1_ConfigBaseDatos.Name = "txt1_ConfigBaseDatos"
        Me.txt1_ConfigBaseDatos.Size = New System.Drawing.Size(180, 29)
        Me.txt1_ConfigBaseDatos.TabIndex = 15
        Me.txt1_ConfigBaseDatos.Text = "VOFICINA"
        Me.txt1_ConfigBaseDatos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Gray
        Me.Label4.Location = New System.Drawing.Point(24, 154)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(180, 24)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "SERVIDOR"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt1_ConfigServer
        '
        Me.txt1_ConfigServer.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ConfigServer.Location = New System.Drawing.Point(24, 181)
        Me.txt1_ConfigServer.Name = "txt1_ConfigServer"
        Me.txt1_ConfigServer.Size = New System.Drawing.Size(180, 29)
        Me.txt1_ConfigServer.TabIndex = 13
        Me.txt1_ConfigServer.Text = "000.000.000.000"
        Me.txt1_ConfigServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(76, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(287, 24)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Ingrese las Credenciales SQL"
        '
        'txt1_Llave
        '
        Me.txt1_Llave.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_Llave.Location = New System.Drawing.Point(199, 75)
        Me.txt1_Llave.Name = "txt1_Llave"
        Me.txt1_Llave.Size = New System.Drawing.Size(100, 29)
        Me.txt1_Llave.TabIndex = 11
        Me.txt1_Llave.Text = "ICC"
        Me.txt1_Llave.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Gray
        Me.Label2.Location = New System.Drawing.Point(142, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(231, 24)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "LLAVE DE SEGURIDAD"
        Me.Label2.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.VOficina_Utilerias.My.Resources.Resources.logo_g
        Me.PictureBox1.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(111, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(303, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Configuración de Archivo Inicio"
        '
        'SFD1
        '
        Me.SFD1.DefaultExt = "dat"
        '
        'utiConfigIni
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(73, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(132, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(484, 361)
        Me.Controls.Add(Me.Panel1)
        Me.KeyPreview = True
        Me.Name = "utiConfigIni"
        Me.Text = "Configura Archivo de Inicio"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Windows.Forms.Panel
    Friend WithEvents txt1_Llave As Windows.Forms.TextBox
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents PictureBox1 As Windows.Forms.PictureBox
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents btn_Ok As Windows.Forms.Button
    Friend WithEvents Label6 As Windows.Forms.Label
    Friend WithEvents txt1_ConfigPass As Windows.Forms.TextBox
    Friend WithEvents Label7 As Windows.Forms.Label
    Friend WithEvents txt1_ConfigUser As Windows.Forms.TextBox
    Friend WithEvents Label5 As Windows.Forms.Label
    Friend WithEvents txt1_ConfigBaseDatos As Windows.Forms.TextBox
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents txt1_ConfigServer As Windows.Forms.TextBox
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents Button1 As Windows.Forms.Button
    Friend WithEvents RichTextBox1 As Windows.Forms.RichTextBox
    Friend WithEvents SFD1 As Windows.Forms.SaveFileDialog
End Class
