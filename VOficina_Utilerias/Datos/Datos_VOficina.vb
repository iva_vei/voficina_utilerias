﻿Imports System.Data.SqlClient
Public Class Datos_VOficina
    Private Const SP_UTIL_ACTUALIZA_FUM As String = "sp_Util_Actualiza_FUM"
    Private Const SP_UTIL_GUARDA_DATOS_TABLA As String = "sp_Util_Guarda_Datos_Tabla"
    Private Const SP_UTIL_RECUPERA_DATOS_TABLA As String = "sp_Util_Recupera_Datos_Tabla"
    Private Const SP_UTIL_RECUPERA_DATOS_TABLA_FUM As String = "sp_Util_Recupera_Datos_Tabla_Fum"
    Private Const SP_UTIL_RECUPERA_ESTRUCTURA_TABLA As String = "sp_Util_Recupera_Estructura_Tabla"
    Private Const SP_UTIL_RECUPERA_FUM As String = "sp_Util_Recupera_FUM"
    Private Const SP_ICC_SYNC_PREPROCESOS As String = "sp_ICC_Sync_Preprocesos"
    Private Const SP_UTIL_RECUPERA_PARAMETROSSYNC As String = "sp_Util_Recupera_ParametrosSync"
    Private Const SP_UTIL_INSERTA_PARAMETROSSYNC As String = "sp_Util_Inserta_ParametrosSync"
    Private Const SP_RECUPERA_SYNC_EJECUTA_INSTRUCCIONES As String = "sp_Recupera_Sync_Ejecuta_Instrucciones"
    Private Const SP_ACTUALIZA_SYNC_EJECUTA_INSTRUCCIONES As String = "sp_Actualiza_Sync_Ejecuta_Instrucciones"
    Public Function Test(ByVal cn As SqlConnection, ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        ''Dim cn As SqlConnection = Nothing
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim dtDatos As DataTable

        Try
            strCommand.CommandText = "SELECT valor = 1"
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = False
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                Else
                    Resultado = False
                End If
            End If
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function SQLQuery(ByVal cn As SqlConnection, ByVal sSQLquery As String, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        ''Dim dtDatos As DataTable

        Try
            strCommand.CommandText = sSQLquery
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = True
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                Else
                    Resultado = False
                End If
            End If
            Excepcion = "NO_ERROR"

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function
    Public Function SQLQuery(ByVal Ref_Cn As String, ByVal sSQLquery As String, ByRef dtDatos As DataTable, ByRef Excepcion As String) As Boolean
        Dim Resultado As Boolean = False
        Dim strCommand As New SqlCommand()
        Dim sADa As New SqlDataAdapter
        Dim dts As New DataSet
        Dim cn As SqlConnection = Nothing
        ''Dim dtDatos As DataTable

        Try
            cn = Globales.oAmbientes.cnx_Conexiones(Ref_Cn).ObtenerConexion()
            strCommand.CommandText = sSQLquery
            strCommand.CommandType = CommandType.Text
            strCommand.Connection = cn

            cn.Open()

            sADa.SelectCommand = strCommand
            sADa.Fill(dts)

            Resultado = True
            Excepcion = "NO_ERROR"
            If dts.Tables.Count > 0 Then
                dtDatos = dts.Tables(0)
                If dtDatos.Rows.Count > 0 Then
                    Resultado = True
                    Excepcion = "NO_ERROR"
                Else
                    Resultado = False
                    Excepcion = "No existen datos."
                End If
            End If

        Catch ex As Exception
            Excepcion = ex.Message
            Resultado = False
        Finally

            If cn IsNot Nothing AndAlso cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If
            If strCommand IsNot Nothing Then
                strCommand.Dispose()
            End If

            strCommand = Nothing
            cn = Nothing
        End Try
        Return Resultado
    End Function

End Class