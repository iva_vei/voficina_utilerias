﻿Imports System.Data.SqlClient
'Imports Microsoft.Office.Interop

Namespace Globales
    Enum e_TIPODATO
        ALFANUMERICO = 0
        NUMERICO = 1
        FECHA = 2
        NUMERICO1 = 3
    End Enum
    Enum TipoABC
        Indice
        Nuevo
        Consulta
        Modificar
        Eliminar
        Autorizar
    End Enum
    Public Enum TipoCombo
        Areas
        Fecha
        Proveedores
        Pedidos
        Ruta
        Sucursales
        Sucursales_CEDIS
        Ubicaciones_Mesa
    End Enum

    Public Class oAmbientes
        Public Shared cnx_Control As clsConexion
        Public Shared cnx_Conexiones As Dictionary(Of String, clsConexion)
        ''Public Shared oUsuario As clsUsuarios
        Public Shared oFormulario As clsFormularios
        Public Shared PinPadBanco As String
        Public Shared Sistema As String
        Public Shared Version As String
        Public Shared SW_Version As Boolean
        Public Shared SW_Produccion As Boolean
        Public Shared FechaEjecucion As Date = Today
        Public Shared Servidor As String
        Public Shared Id_Empresa As String
        Public Shared Empresa As String
        Public Shared BaseDatos As String
        Public Shared Id_Sucursal As String
        Public Shared Jerarquia01 As String = ""
        Public Shared Jerarquia02 As String = ""
        Public Shared Jerarquia03 As String = ""
        Public Shared Jerarquia04 As String = ""

        Public Shared Function CargarXmlCnx(ByVal RutaXml As String, ByVal XmlRuta As String, ByRef Msj As String) As Boolean
            Dim XmlRuta_Local_XSucursal As String = ""
            Dim SW_ArchivoINI As Boolean = False
            Dim xmlDoc As System.Xml.XmlDocument
            Dim sXml As String
            Dim RichTextBox1 As New System.Windows.Forms.RichTextBox

            Dim sServidor As String = ""
            Dim sBaseDAtos As String = ""
            Dim sUser As String = ""
            Dim sPass As String = ""

            Dim oDatos As Datos_VOficina
            Dim dtDatos As DataTable = Nothing
            Dim Cnx As Globales.clsConexion
            Try

                xmlDoc = New System.Xml.XmlDocument()
                Try
                    RichTextBox1.LoadFile(RutaXml & "\" & XmlRuta)
                    sXml = CryptoZ.Decrypt(RichTextBox1.Text, "ICC")
                    xmlDoc.LoadXml(sXml)
                    SW_ArchivoINI = True
                Catch ex As Exception
                    SW_ArchivoINI = False
                    System.Windows.Forms.MessageBox.Show("No se encontro el Archivo INI LOCAL para tener acceso al Sistema." & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                End Try

                If SW_ArchivoINI Then
                    Globales.oAmbientes.SW_Produccion = False
                    Globales.oAmbientes.FechaEjecucion = Today

                    sServidor = xmlDoc.SelectNodes("archivo_inicio/sql/server").Item(0).InnerText
                    sBaseDAtos = xmlDoc.SelectNodes("archivo_inicio/sql/basedatos").Item(0).InnerText
                    sUser = xmlDoc.SelectNodes("archivo_inicio/sql/usuario").Item(0).InnerText
                    sPass = xmlDoc.SelectNodes("archivo_inicio/sql/contrasena").Item(0).InnerText

                    Globales.oAmbientes.SW_Produccion = True

                    Globales.oAmbientes.cnx_Control.cnx_Seguridad(sServidor, sBaseDAtos, sUser, sPass)

                    Globales.oAmbientes.Servidor = sServidor
                    Globales.oAmbientes.BaseDatos = sBaseDAtos

                    oDatos = New Datos_VOficina

                    SW_ArchivoINI = oDatos.Test(Globales.oAmbientes.cnx_Control.ObtenerConexion(), Msj)
                    If SW_ArchivoINI Then
                        If oDatos.SQLQuery(Globales.oAmbientes.cnx_Control.ObtenerConexion(), "SELECT * FROM conexiones C(NOLOCK) WHERE ref1 = 'DLL' AND ref2 != ''", dtDatos, Msj) Then
                            If dtDatos.Rows.Count > 0 Then
                                For Each dtRen As DataRow In dtDatos.Rows
                                    Cnx = New Globales.clsConexion
                                    Cnx.cnx_Seguridad(dtRen.Item("servidor"), dtRen.Item("basedatos"), dtRen.Item("usuario"), dtRen.Item("password"))

                                    Globales.oAmbientes.cnx_Conexiones.Add(dtRen.Item("ref2").ToString.ToUpper, Cnx)
                                Next
                            Else
                                System.Windows.Forms.MessageBox.Show("No se encontraron Conexiones configuradas Ref1 = 'DLL' y ref2 != ''", Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                            End If
                        Else
                            System.Windows.Forms.MessageBox.Show("No se puede conectar al servidor(" & Globales.oAmbientes.Servidor & ")" & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                        End If
                    Else
                        System.Windows.Forms.MessageBox.Show("No se puede conectar al servidor(" & Globales.oAmbientes.Servidor & ")" & vbNewLine & Msj, Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
                    End If
                Else
                End If
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show("No se encontro el Archivo  para tener acceso al Sistema." & ex.Message, Globales.oAmbientes.oFormulario.Tit_Aviso, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
            End Try
            Return SW_ArchivoINI
        End Function

        ''Public Shared Function Crypt(ByVal Cadena As String, Optional ByVal Factor As Double = 34567) As String
        ''    'Definir la Clase de Encriptamiento
        ''    'que proviene del Encrypt.Dll
        ''    'que se encuentra en \VisCoi\Clases\Svc\Encrypt
        ''    Dim Encrypt = New EncryptNet.cEncrypt
        ''    With Encrypt
        ''        .Texto = Cadena
        ''        .Accion = 1
        ''        .Clave = Factor '34567 'Segunda Parte de la Clave
        ''        Crypt = .f_Encrypt
        ''    End With
        ''    Encrypt = Nothing
        ''End Function
        ''' <summary>
        ''' Función que actualiza en regedit la ruta de la ultima version de click once para el inicio automatico al arrancar Windows
        ''' </summary>
        ''' <remarks>IVA</remarks>        ''' 
        Public Shared Sub SaveSettingInicioAutomatico(ByVal opcion As Integer, ByVal appName As String, ByVal section As String, ByVal key As String, ByVal setting As String)
            ' Los datos se guardan en: 

            Const RAMA_WINDOWS_RUN As String = "Software\Microsoft\Windows\CurrentVersion\Run\"
            Const RAMA_WINDOWS_RUNCITRIX As String = "Software\Microsoft\Windows NT\CurrentVersion\Run\"

            If opcion = 0 Then
                Dim rk1 As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(RAMA_WINDOWS_RUN)
                rk1.SetValue(key, setting)
            Else
                Dim rk2 As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(RAMA_WINDOWS_RUNCITRIX)
                rk2.SetValue(key, setting)
            End If

        End Sub
        ''' <summary>
        ''' Función que actualiza en regedit la ruta de la ultima version de click once para el inicio automatico al arrancar Windows
        ''' </summary>
        ''' <remarks>IVA</remarks>        ''' 
        Public Shared Function Valor(ByVal Cantidad As Object) As Double
            If IsDBNull(Cantidad) Then
                Valor = 0.0
            Else
                If Cantidad.ToString.Contains(",") Then
                    Cantidad = Cantidad.ToString().Replace(",", "")
                End If
                If Cantidad.ToString.Contains("%") Then
                    Cantidad = Cantidad.ToString().Replace("%", "")
                End If
                If Cantidad.ToString.Contains("$") Then
                    Cantidad = Cantidad.ToString().Replace("$", "")
                End If
                If IsNumeric(Cantidad) Then
                    Valor = Val(Cantidad)
                Else
                    Valor = 0.0
                End If
            End If
        End Function
        Public Shared Function LeerArchivoBin(ByVal sRuta As String) As Byte()
            Dim bDatos As Byte()
            Dim fInfo As New System.IO.FileInfo(sRuta)
            Dim numBytes As Long = fInfo.Length
            Using ArchivoStream = New System.IO.FileStream(sRuta, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Using ArchivoBin As New System.IO.BinaryReader(ArchivoStream)
                    bDatos = Nothing
                    bDatos = ArchivoBin.ReadBytes(numBytes)
                    ArchivoBin.Close()
                    Return bDatos
                End Using
            End Using
        End Function
        Public Shared Function LeerArchivoBin(ByVal Imagen As System.Drawing.Image) As Byte()
            Dim bDatos As Byte()
            Using ms As System.IO.MemoryStream = New System.IO.MemoryStream
                Imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg) ' Use appropriate format here
                bDatos = ms.ToArray()
                Return bDatos
            End Using
        End Function
        Public Shared Function LeerImagenSegura(ByVal sRuta As String) As System.Drawing.Image
            Using ArchivoStream = New System.IO.FileStream(sRuta, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Return System.Drawing.Image.FromStream(ArchivoStream)
            End Using
        End Function
        Public Shared Function EnviarArchivosFTP(ftpAddress As String, ftpUser As String, ftpPassword As String,
                               fileToUpload As String, targetFileName As String,
                               deleteAfterUpload As Boolean,
                               ExceptionInfo As Exception) As Boolean

            Dim credential As System.Net.NetworkCredential

            Try
                credential = New System.Net.NetworkCredential(ftpUser, ftpPassword)

                If ftpAddress.EndsWith("/") = False Then ftpAddress = ftpAddress & "/"

                Dim sFtpFile As String = ftpAddress & fileToUpload

                Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(sFtpFile), System.Net.FtpWebRequest)

                request.KeepAlive = False
                request.Method = System.Net.WebRequestMethods.Ftp.UploadFile
                request.Credentials = credential
                request.UsePassive = False
                request.Timeout = (60 * 1000) * 3 '3 mins

                Using reader As New IO.FileStream(fileToUpload, IO.FileMode.Open)

                    Dim buffer(Convert.ToInt32(reader.Length - 1)) As Byte
                    reader.Read(buffer, 0, buffer.Length)
                    reader.Close()

                    request.ContentLength = buffer.Length
                    Dim stream As IO.Stream = request.GetRequestStream
                    stream.Write(buffer, 0, buffer.Length)
                    stream.Close()

                    Using response As System.Net.FtpWebResponse = DirectCast(request.GetResponse, System.Net.FtpWebResponse)

                        If deleteAfterUpload Then
                            My.Computer.FileSystem.DeleteFile(fileToUpload)
                        End If

                        response.Close()
                    End Using

                End Using

                Return True

            Catch ex As Exception
                ExceptionInfo = ex
                Return False
            Finally

            End Try

        End Function
        Public Shared Function ImagenResize(vImagen As System.Drawing.Image, vWidth As Integer, vHeight As Integer, Sw_MantenerAspecto As Boolean) As System.Drawing.Image
            Dim Imagen As System.Drawing.Image
            Dim newWidth As Integer
            Dim newHeight As Integer
            Dim originalWidth As Integer
            Dim originalHeight As Integer
            Dim percentWidth As Double
            Dim percentHeight As Double
            Dim percent As Double
            If Sw_MantenerAspecto Then
                originalWidth = vImagen.Width
                originalHeight = vImagen.Height
                percentWidth = CDbl(vWidth) / CDbl(originalWidth)
                percentHeight = CDbl(vHeight) / CDbl(originalHeight)
                percent = IIf(percentHeight < percentWidth, percentHeight, percentWidth)
                newWidth = CInt(originalWidth * percent)
                newHeight = CInt(originalHeight * percent)
            Else
                newWidth = vWidth
                newHeight = vHeight
            End If

            Imagen = New System.Drawing.Bitmap(vImagen, New System.Drawing.Size(newWidth, newHeight))
            Return Imagen.Clone()
        End Function
        Public Shared Function ImportarExcel(ByVal cxn_str As String, ByVal hoja As String, ByVal rango As String, ByRef dtTabla As DataTable, ByRef Msj As String) As Boolean
            Dim regresa As Boolean = False
            Try
                'declarando las variables
                Dim MyConnection As System.Data.OleDb.OleDbConnection
                Dim DtSet As System.Data.DataSet
                Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
                'declarando variable de conexión
                MyConnection = New System.Data.OleDb.OleDbConnection _
                (cxn_str)
                'creando consulta para extraer contenido del archivo de excel y ejecutandola
                MyCommand = New System.Data.OleDb.OleDbDataAdapter _
                    ("select * from [" & hoja & "$" & rango & "]", MyConnection)
                MyCommand.TableMappings.Add("Table", "TestTable")
                DtSet = New System.Data.DataSet
                MyCommand.Fill(DtSet)
                'tabla.DataSet = DtSet.
                'ubicando el resultado en el datagridview
                dtTabla = DtSet.Tables(0)
                'cerrando conexión
                MyConnection.Close()

                regresa = True
                Msj = "Exito"
            Catch ex As Exception
                'imprimiendo mensaje de error
                regresa = False
                Msj = ex.ToString
            End Try
            Return regresa
        End Function
    End Class
    Public Class clsConexion
        Private strServer As String = ""
        Private strDb As String = ""
        Private strUser As String = ""
        Private strPass As String = ""
        Public Function cnx_Seguridad(ByVal Server As String, ByVal DB As String, ByVal user As String, ByVal pass As String) As String
            Dim cnx As String
            strServer = Server
            strDb = DB
            strUser = user
            strPass = pass
            cnx = "Data Source =" & strServer.Trim & ";" _
                        & "Initial Catalog=" & strDb.Trim & ";" _
                        & "Persist Security Info=False;" _
                        & "User ID=" & strUser.Trim & ";" _
                        & "Password=" & strPass.Trim & ";" _
                        & "Packet Size = 4096; Max Pool Size=200;" _
                        & "Connect Timeout=45"
            Return cnx
        End Function
        Private Function cnx_Seguridad() As String
            Dim cnx As String
            cnx = "Data Source =" & strServer.Trim & ";" _
                        & "Initial Catalog=" & strDb.Trim & ";" _
                        & "Persist Security Info=False;" _
                        & "User ID=" & strUser.Trim & ";" _
                        & "Password=" & strPass.Trim & ";" _
                        & "Packet Size = 4096; Max Pool Size=200;" _
                        & "Connect Timeout=45"
            Return cnx
        End Function
        Public ReadOnly Property ObtenerConexion() As SqlConnection
            Get
                Dim cnx As SqlConnection
                cnx = New SqlConnection(cnx_Seguridad)
                Return cnx
            End Get
        End Property
        Public ReadOnly Property ObtenerServer() As String
            Get
                Return strServer
            End Get
        End Property
        Public ReadOnly Property ObtenerDb() As String
            Get
                Return strDb
            End Get
        End Property
        Public ReadOnly Property ObtenerUser() As String
            Get
                Return strUser
            End Get
        End Property
        Public ReadOnly Property ObtenerPass() As String
            Get
                Return strPass
            End Get
        End Property
    End Class
    Public Class clsFormularios
        Friend WithEvents Sfd_Dialogo As System.Windows.Forms.SaveFileDialog
        Friend WithEvents Ofd_Dialogo As System.Windows.Forms.OpenFileDialog
        ''Friend WithEvents Link As DevExpress.XtraPrinting.PrintableComponentLink
        ''Friend WithEvents Ps As DevExpress.XtraPrinting.PrintingSystemBase

        Private Shared _msj_ultimonivel As String
        Private Shared _msj_error As String
        Private Shared _tit_collapse As String
        Private Shared _tit_aviso As String
        Private Shared _frm_Actual As String
        Private Shared _frm_Anterior As String
        Private Shared _frm_Principal As String
        Private Shared _msj_sinregistros As String
        Private Shared _msj_excepcion As String
        Private Shared _msj_error_combo As String

        Private ReadOnly Sw_Grid_Filtros As Boolean = False

        ''Private ReadOnly Formatos As New Excel_FormatoCollection
        Private ImagenEjemplo As System.Drawing.Image
        Public Sub New()
            Msj_UltimoNivel = "No hay más profundidades para esta opción"
            Tit_Collapse = "De CLICK para Desplegar u Ocultar Filtros"
            Msj_Error = "Se generó una excepción:"
            Tit_Aviso = "Aviso del Sistema"
            Frm_Anterior = ""
            Frm_Actual = ""
            Frm_Principal = "IND_TABDIRECCIONH"
            Msj_Excepcion = "Hubo un error al buscar la información: "
            Msj_SinRegistros = "No se encontró información"
            Msj_error_combo = "El valor #COMBO# no se encuentra en la lista "
            'Msj_sinacceso = "El usuario #USUARIO# con el perfil de seguridad #PERFIL#," & vbNewLine & _
            '                "No tiene acceso a la forma #FORMA# del Sistema " & Globales.oAmbientes.Sistema & vbNewLine & _
            '                "Consulte con Sistemas Operaciones, los permisos asignandos para esta pantalla."

        End Sub

        Public Property Msj_Excepcion() As String
            Get
                Return _msj_excepcion
            End Get
            Set(ByVal value As String)
                _msj_excepcion = value
            End Set
        End Property

        Public Property Msj_SinRegistros() As String
            Get
                Return _msj_sinregistros
            End Get
            Set(ByVal value As String)
                _msj_sinregistros = value
            End Set
        End Property


        Public Property Frm_Principal() As String
            Get
                Return _frm_Principal
            End Get
            Set(ByVal value As String)
                _frm_Principal = value
            End Set
        End Property

        Public Property Frm_Anterior() As String
            Get
                Return _frm_Anterior
            End Get
            Set(ByVal value As String)
                _frm_Anterior = value
            End Set
        End Property

        Public Property Frm_Actual() As String
            Get
                Return _frm_Actual
            End Get
            Set(ByVal value As String)
                _frm_Actual = value
            End Set
        End Property

        Public Property Msj_Error() As String
            Get
                Return _msj_error
            End Get
            Set(ByVal value As String)
                _msj_error = value
            End Set
        End Property

        Public Property Msj_UltimoNivel() As String
            Get
                Return _msj_ultimonivel
            End Get
            Set(ByVal value As String)
                _msj_ultimonivel = value
            End Set
        End Property

        Public Property Msj_error_combo() As String
            Get
                Return _msj_error_combo
            End Get
            Set(ByVal value As String)
                _msj_error_combo = value
            End Set
        End Property
        Public Property Tit_Collapse() As String
            Get
                Return _tit_collapse
            End Get
            Set(ByVal value As String)
                _tit_collapse = value
            End Set
        End Property

        Public Property Tit_Aviso() As String
            Get
                Return _tit_aviso
            End Get
            Set(ByVal value As String)
                _tit_aviso = value
            End Set
        End Property
        ''Public ReadOnly Property Msj_sinacceso(ByVal Pantalla As String) As String
        ''    Get
        ''        Dim Msj As String = "El usuario #ID_USUARIO# con el perfil de seguridad #PERFIL#," & vbNewLine &
        ''                    "No tiene acceso a la forma #FORMA# del Sistema " & Globales.oAmbientes.Sistema & vbNewLine &
        ''                    "Consulte con Sistemas Operaciones, los permisos asignandos para esta pantalla."
        ''
        ''        Msj = Msj.Replace("#ID_USUARIO#", Globales.oAmbientes.oUsuario.Id_usuario)
        ''        Msj = Msj.Replace("#PERFIL#", Globales.oAmbientes.oUsuario.Perfil)
        ''        Msj = Msj.Replace("#FORMA#", Pantalla)
        ''
        ''        Return Msj
        ''    End Get
        ''End Property
        ''Public ReadOnly Property Msj_sinacceso(ByVal Pantalla As String, ByVal Permiso As String) As String
        ''    Get
        ''        Dim Msj As String = "El usuario #ID_USUARIO# con el perfil de seguridad #PERFIL#," & vbNewLine &
        ''                    "No cuenta con el permiso #PERMISO# en la forma #FORMA# del Sistema " & Globales.oAmbientes.Sistema & vbNewLine &
        ''                    "Consulte con Sistemas Operaciones, los permisos asignandos para esta pantalla."
        ''
        ''        Msj = Msj.Replace("#ID_USUARIO#", Globales.oAmbientes.oUsuario.Id_usuario)
        ''        Msj = Msj.Replace("#PERFIL#", Globales.oAmbientes.oUsuario.Perfil)
        ''        Msj = Msj.Replace("#FORMA#", Pantalla)
        ''        Msj = Msj.Replace("#PERMISO#", Permiso)
        ''
        ''        Return Msj
        ''    End Get
        ''End Property
        Public Property ImagenEjemplo1 As System.Drawing.Image
            Get
                Return ImagenEjemplo
            End Get
            Set(value As System.Drawing.Image)
                ImagenEjemplo = value
            End Set
        End Property

        ''' <summary>
        ''' Obtiene el valor de la celda seleccionada del grid, espeficando el nombre de la columna (fieldname)
        ''' </summary>
        ''' <param name="Name">Nombre de la Forma</param>
        ''' <param name="Text">Text de la Forma</param>
        ''' <param name="Orientacion"> 1 - Vertical
        '''                            0 - Horizontal </param>
        ''' <param name="Tipo_Hoja">Tamaño de la Hoja</param>
        ''' <param name="parm_sCabecero_Izq">Cabecero situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sCabecero_Cen">Cabecero situado en la parte central de la hoja</param>
        ''' <param name="parm_sCabecero_Der">Cabecero situado en la parte derecha de la hoja</param>
        ''' <param name="parm_sPiePag_Izq">Pie de Pagina situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sPiePag_Cen">Pie de Pagina situado en la parte central de la hoja</param>
        ''' <param name="parm_sPiePag_Der">Pie de Pagina situado en la parte derecha de la hoja</param>
        ''' <returns>True - si genero la reporte</returns>
        ''' <remarks>Ing. Ivan Valdes Acosta</remarks>
        ''Public Function Imprimir(ByVal Name As String, ByVal Text As String, ByVal Grid As DevExpress.XtraGrid.GridControl _
        ''                     , Optional ByVal Orientacion As Integer = 1 _
        ''                     , Optional ByVal Tipo_Hoja As System.Drawing.Printing.PaperKind = Printing.PaperKind.Tabloid _
        ''                     , Optional ByVal parm_sCabecero_Izq As String = "" _
        ''                     , Optional ByVal parm_sCabecero_Cen As String = "" _
        ''                     , Optional ByVal parm_sCabecero_Der As String = "" _
        ''                     , Optional ByVal parm_sPiePag_Izq As String = "" _
        ''                     , Optional ByVal parm_sPiePag_Cen As String = "" _
        ''                     , Optional ByVal parm_sPiePag_Der As String = "" _
        ''                     , Optional ByVal Magen_Top As Integer = 50 _
        ''                     , Optional ByVal Magen_Bottom As Integer = 5 _
        ''                     , Optional ByVal Magen_Left As Integer = 10 _
        ''                     , Optional ByVal Magen_Right As Integer = 10 _
        ''                     , Optional ByVal Escala As Single = 0.9) As Boolean
        ''
        ''    'If Not Entidades.CN_Globales.Usuario.ValidaPermiso(Name, "IMPRIMIR") Then
        ''    '    MsgBox(Entidades.Formularios.Msj_sinacceso(Name, "IMPRIMIR"), MsgBoxStyle.OkOnly, Name)
        ''    '    Return False
        ''    'End If
        ''    Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
        ''    Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink
        ''
        ''    Dim sPage_N_M As String = "Pág [Page #]"
        ''    Dim sDate As String = Format(Now, "dd/MMM/yyyy")
        ''    Dim sTime As String = Format(Now, "hh:mm:ss")
        ''    Dim sUser As String = "[User Name]"
        ''
        ''    Dim Cabecero_Izq As String = IIf(parm_sCabecero_Izq <> "", parm_sCabecero_Izq, (Name))
        ''    Dim Cabecero_Cen As String = IIf(parm_sCabecero_Cen <> "", ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text & vbNewLine & parm_sCabecero_Cen), ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text))
        ''    Dim Cabecero_Der As String = IIf(parm_sCabecero_Der <> "", (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime) & vbNewLine & parm_sCabecero_Der, (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime))
        ''
        ''    Dim PiePag_Izq As String = IIf(parm_sPiePag_Izq <> "", parm_sPiePag_Izq, (""))
        ''    Dim PiePag_Cen As String = IIf(parm_sPiePag_Cen <> "", parm_sPiePag_Cen, (""))
        ''    Dim PiePag_Der As String = IIf(parm_sPiePag_Der <> "", parm_sPiePag_Der, (sUser))
        ''
        ''    Dim GridAllowCellMerge As Boolean
        ''    Try
        ''        'Ps.Begin()
        ''        Select Case Grid.MainView.GetType.Name
        ''            Case "BandedGridView"
        ''                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
        ''                ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
        ''            Case "GridView"
        ''                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
        ''                ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
        ''            Case "AdvBandedGridView"
        ''                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
        ''                'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
        ''        End Select
        ''        Dim phf_encabezado_pie = New DevExpress.XtraPrinting.PageHeaderFooter(New DevExpress.XtraPrinting.PageHeaderArea(New String() {Cabecero_Izq, Cabecero_Cen, Cabecero_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near), New DevExpress.XtraPrinting.PageFooterArea(New String() {PiePag_Izq, PiePag_Cen, PiePag_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near))
        ''        'Link.Images.Add(My.Resources.Requisicion)
        ''        'Dim Brick As DevExpress.XtraPrinting.TextBrick = e.Graph.DrawString(reportHeader, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None)
        ''
        ''        If Orientacion = 1 Then
        ''            Link.Landscape = True
        ''        Else
        ''            Link.Landscape = False
        ''        End If
        ''
        ''        Ps.Document.ScaleFactor = Escala
        ''
        ''        Link.PaperKind = Tipo_Hoja
        ''        Link.Margins.Top = Magen_Top
        ''        Link.Margins.Bottom = Magen_Bottom
        ''        Link.Margins.Left = Magen_Left
        ''        Link.Margins.Right = Magen_Right
        ''        Link.PrintingSystem = Ps
        ''        Link.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
        ''        Link.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
        ''        Link.PrintingSystem.PreviewFormEx.LookAndFeel.SkinName = ""
        ''
        ''        Link.Component = Grid
        ''        Link.PageHeaderFooter = phf_encabezado_pie
        ''
        ''
        ''        Link.CreateDocument()
        ''        'Ps.End()
        ''        Link.PrintingSystem.PreviewFormEx.Show()
        ''
        ''        'Link.ShowPreview()
        ''
        ''        Select Case Grid.MainView.GetType.Name
        ''            Case "BandedGridView"
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''            Case "GridView"
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''            Case "AdvBandedGridView"
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''        End Select
        ''    Catch ex As Exception
        ''        MsgBox("Error al Imprimir. " & ex.Message, MsgBoxStyle.OkOnly, Name)
        ''        Ps.Dispose()
        ''        Link.Dispose()
        ''    Finally
        ''
        ''    End Try
        ''
        ''    Return True
        ''End Function

        ''' <summary>
        ''' Obtiene el valor de la celda seleccionada del grid, espeficando el nombre de la columna (fieldname)
        ''' </summary>
        ''' <param name="Name">Nombre de la Forma</param>
        ''' <param name="Text">Text de la Forma</param>
        ''' <param name="Orientacion"> 1 - Vertical
        '''                            0 - Horizontal </param>
        ''' <param name="Tipo_Hoja">Tamaño de la Hoja</param>
        ''' <param name="parm_sCabecero_Izq">Cabecero situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sCabecero_Cen">Cabecero situado en la parte central de la hoja</param>
        ''' <param name="parm_sCabecero_Der">Cabecero situado en la parte derecha de la hoja</param>
        ''' <param name="parm_sPiePag_Izq">Pie de Pagina situado en la parte izquierda de la hoja</param>
        ''' <param name="parm_sPiePag_Cen">Pie de Pagina situado en la parte central de la hoja</param>
        ''' <param name="parm_sPiePag_Der">Pie de Pagina situado en la parte derecha de la hoja</param>
        ''' <returns>True - si genero la reporte</returns>
        ''' <remarks>Ing. Ivan Valdes Acosta</remarks>
        ''Public Function Imprimir2(ByVal Name As String, ByVal Text As String, ByVal Grids As Object() _
        ''                     , Optional ByVal Orientacion As Integer = 1 _
        ''                     , Optional ByVal Tipo_Hoja As System.Drawing.Printing.PaperKind = Printing.PaperKind.Letter _
        ''                     , Optional ByVal parm_sCabecero_Izq As String = "" _
        ''                     , Optional ByVal parm_sCabecero_Cen As String = "" _
        ''                     , Optional ByVal parm_sCabecero_Der As String = "" _
        ''                     , Optional ByVal parm_sPiePag_Izq As String = "" _
        ''                     , Optional ByVal parm_sPiePag_Cen As String = "" _
        ''                     , Optional ByVal parm_sPiePag_Der As String = "" _
        ''                     , Optional ByVal Magen_Top As Integer = 50 _
        ''                     , Optional ByVal Magen_Bottom As Integer = 5 _
        ''                     , Optional ByVal Magen_Left As Integer = 10 _
        ''                     , Optional ByVal Magen_Right As Integer = 10 _
        ''                     , Optional ByVal Escala As Single = 0.9) As Boolean
        ''
        ''    Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink()
        ''    Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
        ''    Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink
        ''
        ''
        ''
        ''    Dim sPage_N_M As String = "Pág [Page #]"
        ''    Dim sDate As String = Format(Now, "dd/MMM/yyyy")
        ''    Dim sTime As String = Format(Now, "hh:mm:ss")
        ''    Dim sUser As String = "[User Name]"
        ''
        ''    Dim Cabecero_Izq As String = IIf(parm_sCabecero_Izq <> "", parm_sCabecero_Izq, (Name))
        ''    Dim Cabecero_Cen As String = IIf(parm_sCabecero_Cen <> "", ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text & vbNewLine & parm_sCabecero_Cen), ("Sistema de información " & Globales.oAmbientes.Sistema & vbNewLine & Text))
        ''    Dim Cabecero_Der As String = IIf(parm_sCabecero_Der <> "", (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime) & vbNewLine & parm_sCabecero_Der, (sPage_N_M & vbNewLine & sDate & vbNewLine & sTime))
        ''
        ''    Dim PiePag_Izq As String = IIf(parm_sPiePag_Izq <> "", parm_sPiePag_Izq, (""))
        ''    Dim PiePag_Cen As String = IIf(parm_sPiePag_Cen <> "", parm_sPiePag_Cen, (""))
        ''    Dim PiePag_Der As String = IIf(parm_sPiePag_Der <> "", parm_sPiePag_Der, (sUser))
        ''
        ''    Dim GridAllowCellMerge As Boolean
        ''    Try
        ''        'Ps.Begin()
        ''        compositeLink.PrintingSystem = Ps
        ''        For Each Grid As Object In Grids
        ''            Link = New DevExpress.XtraPrinting.PrintableComponentLink()
        ''
        ''            If Orientacion = 1 Then
        ''                Link.Landscape = True
        ''            Else
        ''                Link.Landscape = False
        ''            End If
        ''            Link.PaperKind = Tipo_Hoja
        ''            Link.Margins.Top = Magen_Top
        ''            Link.Margins.Bottom = Magen_Bottom
        ''            Link.Margins.Left = Magen_Left
        ''            Link.Margins.Right = Magen_Right
        ''            Link.PrintingSystem = Ps
        ''            Link.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
        ''            Link.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
        ''            Link.PrintingSystem.PreviewFormEx.LookAndFeel.SkinName = ""
        ''
        ''            Ps.Document.ScaleFactor = Escala
        ''
        ''            If Grid.GetType.Name = "DevExpress.XtraGrid.GridControl" Then
        ''                'DevExpress.XtraGrid.GridControl In Grids
        ''
        ''
        ''                Select Case Grid.MainView.GetType.Name
        ''                    Case "BandedGridView"
        ''                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
        ''                        ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
        ''                    Case "GridView"
        ''                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
        ''                        ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = Sw_Grid_Filtros
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
        ''                    Case "AdvBandedGridView"
        ''                        GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
        ''                        'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                        TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.AutoWidth = IIf(Escala = 1, True, False)
        ''                End Select
        ''
        ''                Link.Component = Grid
        ''                compositeLink.Links.Add(Link)
        ''            Else
        ''                Link.Component = Grid
        ''                compositeLink.Links.Add(Link)
        ''            End If
        ''
        ''
        ''        Next
        ''
        ''        Dim phf_encabezado_pie = New DevExpress.XtraPrinting.PageHeaderFooter(New DevExpress.XtraPrinting.PageHeaderArea(New String() {Cabecero_Izq, Cabecero_Cen, Cabecero_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near), New DevExpress.XtraPrinting.PageFooterArea(New String() {PiePag_Izq, PiePag_Cen, PiePag_Der}, New System.Drawing.Font("Tahoma", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), DevExpress.XtraPrinting.BrickAlignment.Near))
        ''        Link.PageHeaderFooter = phf_encabezado_pie
        ''
        ''        If Orientacion = 1 Then
        ''            compositeLink.Landscape = True
        ''        Else
        ''            compositeLink.Landscape = False
        ''        End If
        ''        compositeLink.PaperKind = Tipo_Hoja
        ''        compositeLink.Margins.Top = Magen_Top
        ''        compositeLink.Margins.Bottom = Magen_Bottom
        ''        compositeLink.Margins.Left = Magen_Left
        ''        compositeLink.Margins.Right = Magen_Right
        ''        compositeLink.PrintingSystem = Ps
        ''        compositeLink.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
        ''        compositeLink.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
        ''        compositeLink.PrintingSystem.PreviewFormEx.LookAndFeel.SkinName = ""
        ''
        ''        compositeLink.PageHeaderFooter = phf_encabezado_pie
        ''        compositeLink.CreateDocument()
        ''        'Ps.End()
        ''        Link.PrintingSystem.PreviewFormEx.Show()
        ''
        ''        For Each Grid As DevExpress.XtraGrid.GridControl In Grids
        ''            Select Case Grid.MainView.GetType.Name
        ''                Case "BandedGridView"
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''                Case "GridView"
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''                Case "AdvBandedGridView"
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''            End Select
        ''        Next
        ''    Catch ex As Exception
        ''        MsgBox("Error al Imprimir. " & ex.Message, MsgBoxStyle.OkOnly, Name)
        ''        Ps.Dispose()
        ''        Link.Dispose()
        ''    Finally
        ''
        ''    End Try
        ''
        ''    Return True
        ''End Function

        ''Public Function ExportarXLS(ByVal Name As String, ByVal Text As String, ByVal Grid As DevExpress.XtraGrid.GridControl _
        ''                        , ByVal sAplicacion As String _
        ''                        , ByVal sProductName As String _
        ''                        , ByVal sCompanyName As String _
        ''                        , Optional ByRef SW_Abrir_Automatico As Boolean = True _
        ''                        , Optional ByRef Nombre_arch As String = "" _
        ''                        , Optional ByRef sRuta_Guardar As String = "C:\ViscoiWMS\Temporal") As Boolean
        ''
        ''    Dim sArchivo As String = ""
        ''
        ''    If Not System.IO.Directory.Exists(sRuta_Guardar) Then
        ''        System.IO.Directory.CreateDirectory(sRuta_Guardar)
        ''    End If
        ''
        ''    Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
        ''    Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink
        ''    Dim GridAllowCellMerge As Boolean
        ''
        ''    'If Not Entidades.CN_Globales.Usuario.ValidaPermiso(Name, "EXCEL") Then
        ''    '    MsgBox(Entidades.Formularios.Msj_sinacceso(Name, "EXCEL"), MsgBoxStyle.OkOnly, Name)
        ''    '    Return False
        ''    'End If
        ''
        ''    Try
        ''        Select Case Grid.MainView.GetType.Name
        ''            Case "BandedGridView"
        ''                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
        ''                ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''            Case "GridView"
        ''                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
        ''                ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
        ''            Case "AdvBandedGridView"
        ''                GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
        ''                'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''        End Select
        ''
        ''        'Me.Sfd_Dialogo = New System.Windows.Forms.SaveFileDialog
        ''        'Me.Sfd_Dialogo.CheckFileExists = False
        ''        'Me.Sfd_Dialogo.FileName = "SFD_Exporta01"
        ''
        ''        'Sfd_Dialogo.Filter = "Archivo Excel|*.xls"
        ''        'Sfd_Dialogo.FileName = Name & Format(Now(), "yyyyMMdd") & Format(Now(), "hhmmss")
        ''        'If Sfd_Dialogo.ShowDialog() = Windows.Forms.DialogResult.OK And Sfd_Dialogo.FileName <> "" Then
        ''        sArchivo = Name & Format(Now(), "yyyyMMdd") & Format(Now(), "hhmmss") & ".xls"
        ''
        ''        Dim ps As New DevExpress.XtraPrinting.PrintingSystem()
        ''        ps.ExportOptions.Pdf.DocumentOptions.Application = sProductName
        ''        ps.ExportOptions.Pdf.DocumentOptions.Author = sCompanyName
        ''        ps.ExportOptions.Pdf.DocumentOptions.Title = Name
        ''        ''ps.ExportOptions.Image.Resolution
        ''        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ps)
        ''
        ''        link.Component = Grid
        ''        link.Landscape = True
        ''        link.PaperKind = System.Drawing.Printing.PaperKind.LetterExtra
        ''        link.PrintingSystem.PreviewFormEx.LookAndFeel.UseDefaultLookAndFeel = False
        ''        link.PrintingSystem.PreviewFormEx.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
        ''
        ''        link.CreateDocument()
        ''        link.PrintingSystem.ExportToXls(sRuta_Guardar & "\" & sArchivo)
        ''
        ''        'Dim appExcel As New Microsoft.Office.Interop.Excel.Application
        ''        'sAplicacion = appExcel.Path & "\EXCEL.EXE"
        ''
        ''        If SW_Abrir_Automatico Then
        ''            'Shell(sAplicacion & " """ & sRuta_Guardar & "\" & sArchivo & """", vbNormalFocus)
        ''            System.Diagnostics.Process.Start(sRuta_Guardar & "\" & sArchivo)
        ''        End If
        ''        Nombre_arch = sRuta_Guardar & "\" & sArchivo
        ''        'End If
        ''
        ''        Select Case Grid.MainView.GetType.Name
        ''            Case "BandedGridView"
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''            Case "GridView"
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''            Case "AdvBandedGridView"
        ''                TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''        End Select
        ''    Catch ex As Exception
        ''        MsgBox("Error al Exportar a Excel. " & ex.Message, MsgBoxStyle.OkOnly, Name)
        ''        Ps.Dispose()
        ''        Link.Dispose()
        ''    Finally
        ''        'Me.Sfd_Dialogo.Dispose()
        ''    End Try
        ''    Return True
        ''End Function
        ''Public Function ExportarXLS2(ByVal Name As String, ByVal Text As String, ByVal Grids As DevExpress.XtraGrid.GridControl() _
        ''                        , ByVal sAplicacion As String _
        ''                        , ByVal sProductName As String _
        ''                        , ByVal sCompanyName As String _
        ''                        , Optional ByRef sArchivo As String = "" _
        ''                        , Optional ByRef SW_Abrir_Automatico As Boolean = True _
        ''                        , Optional ByRef Nombre_arch As String = "" _
        ''                        , Optional ByRef sRuta_Guardar As String = "C:\ViscoiWMS\Temporal") As Boolean
        ''
        ''    'Dim sArchivo As String = ""
        ''
        ''    If Not System.IO.Directory.Exists(sRuta_Guardar) Then
        ''        System.IO.Directory.CreateDirectory(sRuta_Guardar)
        ''    End If
        ''
        ''    Me.Ps = New DevExpress.XtraPrinting.PrintingSystem
        ''    Me.Link = New DevExpress.XtraPrinting.PrintableComponentLink
        ''    Dim GridAllowCellMerge As Boolean
        ''
        ''    'If Not Entidades.CN_Globales.Usuario.ValidaPermiso(Name, "EXCEL") Then
        ''    '    MsgBox(Entidades.Formularios.Msj_sinacceso(Name, "EXCEL"), MsgBoxStyle.OkOnly, Name)
        ''    '    Return False
        ''    'End If
        ''
        ''    Try
        ''        sArchivo = Name & Format(Now(), "yyyyMMdd") & Format(Now(), "hhmmss") & ".xls"
        ''        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink()
        ''        Dim ps As New DevExpress.XtraPrinting.PrintingSystem()
        ''        ps.ExportOptions.Pdf.DocumentOptions.Application = sProductName
        ''        ps.ExportOptions.Pdf.DocumentOptions.Author = sCompanyName
        ''        ps.ExportOptions.Pdf.DocumentOptions.Title = Name
        ''
        ''
        ''        compositeLink.PrintingSystem = ps
        ''
        ''        For Each Grid As DevExpress.XtraGrid.GridControl In Grids
        ''
        ''            Select Case Grid.MainView.GetType.Name
        ''                Case "BandedGridView"
        ''                    GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = False
        ''                    ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.PrintFooter = True
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                Case "GridView"
        ''                    GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = False
        ''                    ''TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.PrintFooter = True
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsPrint.EnableAppearanceOddRow = True
        ''                Case "AdvBandedGridView"
        ''                    GridAllowCellMerge = TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = False
        ''                    'TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.ShowFooter = Sw_Grid_Filtros
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.PrintFooter = True
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsPrint.EnableAppearanceOddRow = True
        ''            End Select
        ''
        ''
        ''
        ''            Dim link As New DevExpress.XtraPrinting.PrintableComponentLink()
        ''
        ''
        ''            link.Component = Grid
        ''            link.Landscape = True
        ''            link.PaperKind = System.Drawing.Printing.PaperKind.LetterExtra
        ''
        ''
        ''            compositeLink.Links.Add(link)
        ''
        ''
        ''
        ''        Next
        ''
        ''        compositeLink.CreateDocument()
        ''        compositeLink.PrintingSystem.ExportToXlsx(sRuta_Guardar & "\" & sArchivo)
        ''
        ''        'Dim appExcel As New Microsoft.Office.Interop.Excel.Application
        ''        'sAplicacion = appExcel.Path & "\EXCEL.EXE"
        ''
        ''        If SW_Abrir_Automatico Then
        ''            'Shell(sAplicacion & " """ & sRuta_Guardar & "\" & sArchivo & """", vbNormalFocus)
        ''            System.Diagnostics.Process.Start(sRuta_Guardar & "\" & sArchivo)
        ''        End If
        ''        Nombre_arch = sRuta_Guardar & "\" & sArchivo
        ''        'End If
        ''
        ''
        ''        For Each Grid As DevExpress.XtraGrid.GridControl In Grids
        ''            Select Case Grid.MainView.GetType.Name
        ''                Case "BandedGridView"
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''                Case "GridView"
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.Grid.GridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''                Case "AdvBandedGridView"
        ''                    TryCast(Grid.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).OptionsView.AllowCellMerge = GridAllowCellMerge
        ''            End Select
        ''        Next
        ''
        ''
        ''
        ''    Catch ex As Exception
        ''        MsgBox("Error al Exportar a Excel. " & ex.Message, MsgBoxStyle.OkOnly, Name)
        ''        Ps.Dispose()
        ''        Link.Dispose()
        ''    Finally
        ''        'Me.Sfd_Dialogo.Dispose()
        ''    End Try
        ''    Return True
        ''End Function

        ''Public Sub Estblece8020(ByVal Columna80 As DevExpress.XtraGrid.Columns.GridColumn, ByVal Columna As DevExpress.XtraGrid.Columns.GridColumn, ByRef GridC As DevExpress.XtraGrid.GridControl)
        ''    Dim Total As Double = 0.0
        ''    Dim Total80 As Double = 0.0
        ''    Dim prc80 As Double = 0.0
        ''    'Dim GridV As DevExpress.XtraGrid.Views.Base.BaseView
        ''    'GridV = GridC.MainView
        ''    Try
        ''        Select Case GridC.MainView.GetType.Name
        ''            Case "BandedGridView"
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).ClearSorting()
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).Columns(Columna.FieldName).SortIndex = 0
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).Columns(Columna.FieldName).SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).Columns(Columna.FieldName).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
        ''            Case "GridView"
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).ClearSorting()
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).Columns(Columna.FieldName).SortIndex = 0
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).Columns(Columna.FieldName).SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).Columns(Columna.FieldName).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
        ''            Case "AdvBandedGridView"
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).ClearSorting()
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).Columns(Columna.FieldName).SortIndex = 0
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).Columns(Columna.FieldName).SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).Columns(Columna.FieldName).SortOrder = DevExpress.Data.ColumnSortOrder.Descending
        ''        End Select
        ''
        ''        For iI = 0 To GridC.MainView.RowCount - 1
        ''            Select Case GridC.MainView.GetType.Name
        ''                Case "BandedGridView"
        ''                    Total += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).GetRowCellValue(iI, Columna)
        ''                    TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).SetRowCellValue(iI, Columna80, 0)
        ''                Case "GridView"
        ''                    Total += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).GetRowCellValue(iI, Columna)
        ''                    TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).SetRowCellValue(iI, Columna80, 0)
        ''                Case "AdvBandedGridView"
        ''                    Total += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).GetRowCellValue(iI, Columna)
        ''                    TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).SetRowCellValue(iI, Columna80, 0)
        ''            End Select
        ''        Next
        ''        prc80 = Total * 0.8
        ''
        ''        For iI = 0 To GridC.MainView.RowCount - 1
        ''            If Total80 <= prc80 Then
        ''                Select Case GridC.MainView.GetType.Name
        ''                    Case "BandedGridView"
        ''                        Total80 += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).GetRowCellValue(iI, Columna)
        ''                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).SetRowCellValue(iI, Columna80, 1)
        ''                    Case "GridView"
        ''                        Total80 += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).GetRowCellValue(iI, Columna)
        ''                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).SetRowCellValue(iI, Columna80, 1)
        ''                    Case "AdvBandedGridView"
        ''                        Total80 += TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).GetRowCellValue(iI, Columna)
        ''                        TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).SetRowCellValue(iI, Columna80, 1)
        ''                End Select
        ''            End If
        ''        Next
        ''        Select Case GridC.MainView.GetType.Name
        ''            Case "BandedGridView"
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).FormatRules.Item("Format_8020").ColumnApplyTo = Columna
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.BandedGridView).FocusedRowHandle = 0
        ''            Case "GridView"
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).FormatRules.Item("Format_8020").ColumnApplyTo = Columna
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.Grid.GridView).FocusedRowHandle = 0
        ''            Case "AdvBandedGridView"
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).FormatRules.Item("Format_8020").ColumnApplyTo = Columna
        ''                TryCast(GridC.MainView, DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView).FocusedRowHandle = 0
        ''        End Select
        ''
        ''    Catch ex As Exception
        ''
        ''    End Try
        ''End Sub
        ''Public Shared Sub Grid_Prevenir_Tab(ByRef sender As Object, ByRef e As KeyEventArgs, ByRef Next1 As Object)
        ''    Dim GridC As DevExpress.XtraGrid.GridControl
        ''    Try
        ''        If (e.KeyCode = Keys.Tab) Then
        ''            'GridC = TryCast(sender, DevExpress.XtraGrid.GridControl)
        ''            e.Handled = False
        ''            If Not Next1 Is Nothing Then
        ''                Next1.focus()
        ''            End If
        ''        End If
        ''    Catch ex As Exception
        ''
        ''    End Try
        ''End Sub
    End Class
    Public Class oPaqueterias
        Public Class clsDatosDireccion
            Dim _NoCliente As String
            Dim _empresa As String
            Dim _contacto As String
            Dim _direccion1 As String
            Dim _direccion2 As String
            Dim _ciudad As String
            Dim _Estado As String
            Dim _CodigoPostal As String
            Dim _Colonia As String
            Dim _Telefono As String
            Dim _Celular As String
            Public Sub New()
                _NoCliente = ""
                _empresa = ""
                _contacto = ""
                _direccion1 = ""
                _direccion2 = ""
                _ciudad = ""
                _Estado = ""
                _CodigoPostal = ""
                _Colonia = ""
                _Telefono = ""
                _Celular = ""
            End Sub
            Public Property NoCliente As String
                Get
                    Return _NoCliente
                End Get
                Set(value As String)
                    _NoCliente = value
                End Set
            End Property

            Public Property Empresa As String
                Get
                    Return _empresa
                End Get
                Set(value As String)
                    _empresa = value
                End Set
            End Property

            Public Property Contacto As String
                Get
                    Return _contacto
                End Get
                Set(value As String)
                    _contacto = value
                End Set
            End Property

            Public Property Direccion1 As String
                Get
                    Return _direccion1
                End Get
                Set(value As String)
                    _direccion1 = value
                End Set
            End Property

            Public Property Direccion2 As String
                Get
                    Return _direccion2
                End Get
                Set(value As String)
                    _direccion2 = value
                End Set
            End Property

            Public Property Ciudad As String
                Get
                    Return _ciudad
                End Get
                Set(value As String)
                    _ciudad = value
                End Set
            End Property

            Public Property Estado As String
                Get
                    Return _Estado
                End Get
                Set(value As String)
                    _Estado = value
                End Set
            End Property

            Public Property CodigoPostal As String
                Get
                    Return _CodigoPostal
                End Get
                Set(value As String)
                    _CodigoPostal = value
                End Set
            End Property

            Public Property Colonia As String
                Get
                    Return _Colonia
                End Get
                Set(value As String)
                    _Colonia = value
                End Set
            End Property

            Public Property Telefono As String
                Get
                    Return _Telefono
                End Get
                Set(value As String)
                    _Telefono = value
                End Set
            End Property

            Public Property Celular As String
                Get
                    Return _Celular
                End Get
                Set(value As String)
                    _Celular = value
                End Set
            End Property
        End Class

        '        Public Class oEstafeta
        '            Public Shared Function NuevaGuia(ByVal NoClientePaqueteria As String _
        '                                          , ByVal UserPaqueteria As String _
        '                                          , ByVal PassPaqueteria As String _
        '                                          , ByVal IdPaqueteria As String _
        '                                          , ByVal EtiquetaZebra As Boolean _
        '                                          , ByVal Origen As clsDatosDireccion _
        '                                          , ByVal Destino As clsDatosDireccion _
        '                                          , ByVal Contenido As String _
        '                                          , ByVal Referencia As String _
        '                                          , ByVal InformacionAdicional As String _
        '                                          , ByRef GuiaPaqueteria As String _
        '                                          , ByRef Msj As String) As Boolean
        '                Dim Resultado As Boolean = False
        '#If DEBUG Then
        '                Dim Paq_Solicitud As New Paqueteria_Pruebas.EstafetaLabelRequest
        '                Dim Paq_Response As New Paqueteria_Pruebas.EstafetaLabelResponse
        '                Dim Paq_DescripcionLista = New Paqueteria_Pruebas.LabelDescriptionList
        '                Dim Paq_Destino As New Paqueteria_Pruebas.DestinationInfo
        '                Dim Paq_Origen As New Paqueteria_Pruebas.OriginInfo
        '                Dim Paq_Locator As New Paqueteria_Pruebas.EstafetaLabelClient
        '                Dim Paq_Guia As New Paqueteria_Pruebas.Waybill
        '                Dim Paq_Printer As New Paqueteria_Pruebas.PrinterSystem
        '                Dim listArray(0) As Paqueteria_Pruebas.LabelDescriptionList
        '#Else
        '                Dim Paq_Solicitud As New Paqueteria.EstafetaLabelRequest
        '                Dim Paq_Response As New Paqueteria.EstafetaLabelResponse
        '                Dim Paq_DescripcionLista = New Paqueteria.LabelDescriptionList
        '                Dim Paq_Destino As New Paqueteria.DestinationInfo
        '                Dim Paq_Origen As New Paqueteria.OriginInfo
        '                Dim Paq_Locator As New Paqueteria.EstafetaLabelClient
        '                Dim Paq_Guia As New Paqueteria.Waybill
        '                Dim listArray(0) As Paqueteria.LabelDescriptionList
        '#End If
        '                Try
        '                    Paq_Solicitud.customerNumber = NoClientePaqueteria      ''"0000000"
        '                    Paq_Solicitud.login = UserPaqueteria                    ''"prueba1"
        '                    Paq_Solicitud.password = PassPaqueteria                 ''"lAbeL_K_11"
        '                    Paq_Solicitud.suscriberId = IdPaqueteria                ''"28"
        '                    Paq_Solicitud.quadrant = 0                              ''0
        '                    Paq_Solicitud.paperType = IIf(EtiquetaZebra, 2, 1)      ''2

        '                    ''Informacion Origen       
        '                    Paq_Origen.address1 = Origen.Direccion1         ''AV. JUAREZ 4170
        '                    Paq_Origen.address2 = Origen.Direccion2         ''SALTILLO 400 Y CALLE MEXICO
        '                    Paq_Origen.city = Origen.Ciudad                 ''TORREON
        '                    Paq_Origen.contactName = Origen.Contacto        ''PASCUAL LUCIO
        '                    Paq_Origen.corporateName = Origen.Empresa       ''CONFIASHOP
        '                    Paq_Origen.customerNumber = Origen.NoCliente    ''8668827
        '                    Paq_Origen.neighborhood = Origen.Colonia        ''NUEVA CALIFORNIA
        '                    Paq_Origen.phoneNumber = Origen.Telefono        ''8715724240
        '                    Paq_Origen.cellPhone = Origen.Celular           ''
        '                    Paq_Origen.state = Origen.Estado                ''COAHUILA
        '                    Paq_Origen.zipCode = Origen.CodigoPostal        ''27089

        '                    ''Informacion Destino        
        '                    Paq_Destino.address1 = Destino.Direccion1
        '                    Paq_Destino.address2 = Destino.Direccion2
        '                    Paq_Destino.city = Destino.Ciudad
        '                    Paq_Destino.contactName = Destino.Contacto
        '                    Paq_Destino.corporateName = Destino.Empresa
        '                    Paq_Destino.customerNumber = Destino.NoCliente
        '                    Paq_Destino.neighborhood = Destino.Colonia
        '                    Paq_Destino.phoneNumber = Destino.Telefono
        '                    Paq_Destino.cellPhone = Destino.Celular
        '                    Paq_Destino.state = Destino.Estado
        '                    Paq_Destino.zipCode = Destino.CodigoPostal

        '                    Paq_DescripcionLista.originInfo = Paq_Origen
        '                    Paq_DescripcionLista.destinationInfo = Paq_Destino

        '                    ''Información adicional       
        '                    Paq_DescripcionLista.aditionalInfo = InformacionAdicional
        '                    If Paq_DescripcionLista.aditionalInfo.Length > 25 Then
        '                        Paq_DescripcionLista.aditionalInfo = Paq_DescripcionLista.aditionalInfo.Substring(0, 25)
        '                    End If
        '                    ''Contenido       
        '                    Paq_DescripcionLista.content = Contenido
        '                    ''Referencia      
        '                    Paq_DescripcionLista.reference = IIf(Referencia.Length > 0, Referencia, " ")
        '                    ''Centro de costos       
        '                    Paq_DescripcionLista.costCenter = "VENTALINEA"
        '                    ''Ocurre       
        '                    Paq_DescripcionLista.deliveryToEstafetaOffice = False
        '                    ''En caso de envio a otro pais, solo siglas
        '                    Paq_DescripcionLista.destinationCountryId = "MX"

        '                    ''Tipo de envio 1=SOBRE 4=PAQUETE       
        '                    Paq_DescripcionLista.parcelTypeId = 4
        '                    ''Peso      
        '                    Paq_DescripcionLista.weight = 1
        '                    ''Número de etiquetas solicitadas       
        '                    Paq_DescripcionLista.numberOfLabels = 1
        '                    ''Código postal de Origen para enrutamiento       
        '                    Paq_DescripcionLista.originZipCodeForRouting = Paq_Origen.zipCode
        '                    ''Servicio que se usará      
        '                    Paq_DescripcionLista.serviceTypeId = "70"
        '                    ''Numero de oficina que corresponde al cliente       
        '                    Paq_DescripcionLista.officeNum = "130"
        '                    ''Documento de retorno       
        '                    Paq_DescripcionLista.returnDocument = False
        '                    ''Servicio del documento de retorno       
        '                    Paq_DescripcionLista.serviceTypeIdDocRet = "50"
        '                    ''Fecha de vigencia       
        '                    Paq_DescripcionLista.effectiveDate = (Today.Year * 10000 _
        '                                                                        + Today.Month * 100 _
        '                                                                        + Today.Day * 1).ToString
        '                    ''Descripcion del contenido       
        '                    Paq_DescripcionLista.contentDescription = InformacionAdicional

        '                    'Paq_Printer.systemName = Globales.oAmbientes.Sistema
        '                    'Paq_Printer.systemVersion = Globales.oAmbientes.Version
        '                    'Paq_Printer.systemReleasedDate = "20200101"

        '                    listArray(0) = Paq_DescripcionLista
        '                    Paq_Solicitud.labelDescriptionList = listArray
        '                    'Paq_Solicitud.printerSystem = Paq_Printer

        '                    Paq_Response = Paq_Locator.createLabel(Paq_Solicitud)

        '                    If (Paq_Response.globalResult IsNot Nothing And Paq_Response.labelPDF IsNot Nothing) Then

        '                        ''Preparamos la salida del PDF         
        '                        Dim fout As System.IO.FileStream = Nothing
        '                        Dim aBytes As Byte() = Nothing
        '                        GuiaPaqueteria = Paq_Response.labelResultList(0).resultDescription

        '                        If Not IO.Directory.Exists("C:\Confia\") Then
        '                            IO.Directory.CreateDirectory("C:\Confia")
        '                        End If

        '                        If Not IO.Directory.Exists("C:\Confia\Guias") Then
        '                            IO.Directory.CreateDirectory("C:\Confia\Guias")
        '                        End If

        '                        If Not IO.Directory.Exists("C:\Confia\Guias\Estafeta") Then
        '                            IO.Directory.CreateDirectory("C:\Confia\Guias\Estafeta")
        '                        End If
        '                        fout = System.IO.File.Create("C:\Confia\Guias\Estafeta\" & GuiaPaqueteria & ".pdf", 1024)

        '                        aBytes = Paq_Response.labelPDF
        '                        fout.Write(aBytes, 0, aBytes.Length)
        '                        fout.Close()
        '                        Resultado = True
        '                    Else
        '                        ''Hubo un error y debe ser mostrado  
        '                        Resultado = False
        '                        Msj = Paq_Response.globalResult.resultDescription
        '                    End If
        '                Catch ex As Exception
        '                    Resultado = False
        '                    Msj = ex.Message
        '                End Try
        '                Return Resultado
        '            End Function
        '        End Class
    End Class
End Namespace