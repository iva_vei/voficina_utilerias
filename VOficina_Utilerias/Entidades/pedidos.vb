﻿
Public Class pedidos
    Dim _id_empresa As String
    Dim _id_sucursal As String
    Dim _pedido As String
    Dim _tipo As String
    Dim _afectacion As String
    Dim _fecha As Date
    Dim _proveedor As Integer
    Dim _depto As String
    Dim _estatus As String
    Dim _pendiente As String
    Dim _id_comprador As String
    Dim _condicion As Integer
    Dim _prontopago As Double
    Dim _diasppp As Integer
    Dim _plazo As Integer
    Dim _flete As Double
    Dim _devolucion As String
    Dim _diapago As String
    Dim _entregas As String
    Dim _fechabase As String
    Dim _confirmo As String
    Dim _feccap As Date
    Dim _fecimpr As Date
    Dim _fecentrega As Date
    Dim _feccancela As Date
    Dim _fecrecibo1 As Date
    Dim _fecrecibo2 As Date
    Dim _grupocostos As String
    Dim _renglones As Integer
    Dim _articulos As Integer
    Dim _cantidad As Double
    Dim _neto As Double
    Dim _costo As Double
    Dim _venta As Double
    Dim _oferta As Double
    Dim _impiva As Double
    Dim _impivaofe As Double
    Dim _margenvta As Double
    Dim _margenofe As Double
    Dim _recarticulos As Integer
    Dim _reccantidad As Integer
    Dim _recneto As Double
    Dim _reccosto As Double
    Dim _recventa As Double
    Dim _recoferta As Double
    Dim _observ1 As String
    Dim _observ2 As String
    Dim _formato As String
    Dim _fecfront As Date
    Dim _tcambio1 As Double
    Dim _tcambio2 As Double
    Dim _autonumsuc As String
    Dim _fum As Date
    Dim _id_usuario As String
    Dim _flujo As String
    Dim _moneda As String
    Dim _enviado As Integer
    Dim _fecha_enviado As Date
    Dim _descuento As Double
    Dim _area As String
    Dim _terminado As String
    Dim _usuario_autorizo As String
    Dim _surte As String
    Dim _plazo1 As Integer
    Dim _plazo2 As Integer
    Dim _plazo3 As Integer
    Dim _plazo4 As Integer
    Dim _porc1 As Double
    Dim _porc2 As Double
    Dim _porc3 As Double
    Dim _porc4 As Double
    Dim _aseguradora As String
    Dim _viaembarque As String
    Dim _diasinvant As Integer
    Dim _diasinvnvo As Integer

    Dim _nomcomp As String
    Dim _comprador_nombre As String
    Dim _comprador_clave As String
    Dim _comprador_correo As String
    Dim _comprador_contacto As String
    Dim _suc1_direccion As String
    Dim suc1_ciudad As String
    Dim _suc_entrega_direccion As String
    Dim _suc_entrega_ciudad As String

    Dim _oProveedor As Proveedores
    Dim _Detalle As List(Of PedidosDet)

#Region "PROPIEDADES"
    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Pedido As String
        Get
            Return _pedido
        End Get
        Set(value As String)
            _pedido = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Afectacion As String
        Get
            Return _afectacion
        End Get
        Set(value As String)
            _afectacion = value
        End Set
    End Property

    Public Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(value As Date)
            _fecha = value
        End Set
    End Property

    Public Property Proveedor As Integer
        Get
            Return _proveedor
        End Get
        Set(value As Integer)
            _proveedor = value
        End Set
    End Property

    Public Property Depto As String
        Get
            Return _depto
        End Get
        Set(value As String)
            _depto = value
        End Set
    End Property

    Public Property Estatus As String
        Get
            Return _estatus
        End Get
        Set(value As String)
            _estatus = value
        End Set
    End Property

    Public Property Pendiente As String
        Get
            Return _pendiente
        End Get
        Set(value As String)
            _pendiente = value
        End Set
    End Property

    Public Property Id_comprador As String
        Get
            Return _id_comprador
        End Get
        Set(value As String)
            _id_comprador = value
        End Set
    End Property

    Public Property Condicion As Integer
        Get
            Return _condicion
        End Get
        Set(value As Integer)
            _condicion = value
        End Set
    End Property

    Public Property Prontopago As Double
        Get
            Return _prontopago
        End Get
        Set(value As Double)
            _prontopago = value
        End Set
    End Property

    Public Property Diasppp As Integer
        Get
            Return _diasppp
        End Get
        Set(value As Integer)
            _diasppp = value
        End Set
    End Property

    Public Property Plazo As Integer
        Get
            Return _plazo
        End Get
        Set(value As Integer)
            _plazo = value
        End Set
    End Property

    Public Property Flete As Double
        Get
            Return _flete
        End Get
        Set(value As Double)
            _flete = value
        End Set
    End Property

    Public Property Devolucion As String
        Get
            Return _devolucion
        End Get
        Set(value As String)
            _devolucion = value
        End Set
    End Property

    Public Property Diapago As String
        Get
            Return _diapago
        End Get
        Set(value As String)
            _diapago = value
        End Set
    End Property

    Public Property Entregas As String
        Get
            Return _entregas
        End Get
        Set(value As String)
            _entregas = value
        End Set
    End Property

    Public Property Fechabase As String
        Get
            Return _fechabase
        End Get
        Set(value As String)
            _fechabase = value
        End Set
    End Property

    Public Property Confirmo As String
        Get
            Return _confirmo
        End Get
        Set(value As String)
            _confirmo = value
        End Set
    End Property

    Public Property Feccap As Date
        Get
            Return _feccap
        End Get
        Set(value As Date)
            _feccap = value
        End Set
    End Property

    Public Property Fecimpr As Date
        Get
            Return _fecimpr
        End Get
        Set(value As Date)
            _fecimpr = value
        End Set
    End Property

    Public Property Fecentrega As Date
        Get
            Return _fecentrega
        End Get
        Set(value As Date)
            _fecentrega = value
        End Set
    End Property

    Public Property Feccancela As Date
        Get
            Return _feccancela
        End Get
        Set(value As Date)
            _feccancela = value
        End Set
    End Property

    Public Property Fecrecibo1 As Date
        Get
            Return _fecrecibo1
        End Get
        Set(value As Date)
            _fecrecibo1 = value
        End Set
    End Property

    Public Property Fecrecibo2 As Date
        Get
            Return _fecrecibo2
        End Get
        Set(value As Date)
            _fecrecibo2 = value
        End Set
    End Property

    Public Property Grupocostos As String
        Get
            Return _grupocostos
        End Get
        Set(value As String)
            _grupocostos = value
        End Set
    End Property

    Public Property Renglones As Integer
        Get
            Return _renglones
        End Get
        Set(value As Integer)
            _renglones = value
        End Set
    End Property

    Public Property Articulos As Integer
        Get
            Return _articulos
        End Get
        Set(value As Integer)
            _articulos = value
        End Set
    End Property

    Public Property Cantidad As Double
        Get
            Return _cantidad
        End Get
        Set(value As Double)
            _cantidad = value
        End Set
    End Property

    Public Property Neto As Double
        Get
            Return _neto
        End Get
        Set(value As Double)
            _neto = value
        End Set
    End Property

    Public Property Costo As Double
        Get
            Return _costo
        End Get
        Set(value As Double)
            _costo = value
        End Set
    End Property

    Public Property Venta As Double
        Get
            Return _venta
        End Get
        Set(value As Double)
            _venta = value
        End Set
    End Property

    Public Property Oferta As Double
        Get
            Return _oferta
        End Get
        Set(value As Double)
            _oferta = value
        End Set
    End Property

    Public Property Impiva As Double
        Get
            Return _impiva
        End Get
        Set(value As Double)
            _impiva = value
        End Set
    End Property

    Public Property Impivaofe As Double
        Get
            Return _impivaofe
        End Get
        Set(value As Double)
            _impivaofe = value
        End Set
    End Property

    Public Property Margenvta As Double
        Get
            Return _margenvta
        End Get
        Set(value As Double)
            _margenvta = value
        End Set
    End Property

    Public Property Margenofe As Double
        Get
            Return _margenofe
        End Get
        Set(value As Double)
            _margenofe = value
        End Set
    End Property

    Public Property Recarticulos As Integer
        Get
            Return _recarticulos
        End Get
        Set(value As Integer)
            _recarticulos = value
        End Set
    End Property

    Public Property Reccantidad As Integer
        Get
            Return _reccantidad
        End Get
        Set(value As Integer)
            _reccantidad = value
        End Set
    End Property

    Public Property Recneto As Double
        Get
            Return _recneto
        End Get
        Set(value As Double)
            _recneto = value
        End Set
    End Property

    Public Property Reccosto As Double
        Get
            Return _reccosto
        End Get
        Set(value As Double)
            _reccosto = value
        End Set
    End Property

    Public Property Recventa As Double
        Get
            Return _recventa
        End Get
        Set(value As Double)
            _recventa = value
        End Set
    End Property

    Public Property Recoferta As Double
        Get
            Return _recoferta
        End Get
        Set(value As Double)
            _recoferta = value
        End Set
    End Property

    Public Property Observ1 As String
        Get
            Return _observ1
        End Get
        Set(value As String)
            _observ1 = value
        End Set
    End Property

    Public Property Observ2 As String
        Get
            Return _observ2
        End Get
        Set(value As String)
            _observ2 = value
        End Set
    End Property

    Public Property Formato As String
        Get
            Return _formato
        End Get
        Set(value As String)
            _formato = value
        End Set
    End Property

    Public Property Fecfront As Date
        Get
            Return _fecfront
        End Get
        Set(value As Date)
            _fecfront = value
        End Set
    End Property

    Public Property Tcambio1 As Double
        Get
            Return _tcambio1
        End Get
        Set(value As Double)
            _tcambio1 = value
        End Set
    End Property

    Public Property Tcambio2 As Double
        Get
            Return _tcambio2
        End Get
        Set(value As Double)
            _tcambio2 = value
        End Set
    End Property

    Public Property Autonumsuc As String
        Get
            Return _autonumsuc
        End Get
        Set(value As String)
            _autonumsuc = value
        End Set
    End Property

    Public Property Fum As Date
        Get
            Return _fum
        End Get
        Set(value As Date)
            _fum = value
        End Set
    End Property

    Public Property Id_usuario As String
        Get
            Return _id_usuario
        End Get
        Set(value As String)
            _id_usuario = value
        End Set
    End Property

    Public Property Flujo As String
        Get
            Return _flujo
        End Get
        Set(value As String)
            _flujo = value
        End Set
    End Property

    Public Property Moneda As String
        Get
            Return _moneda
        End Get
        Set(value As String)
            _moneda = value
        End Set
    End Property

    Public Property Enviado As Integer
        Get
            Return _enviado
        End Get
        Set(value As Integer)
            _enviado = value
        End Set
    End Property

    Public Property Fecha_enviado As Date
        Get
            Return _fecha_enviado
        End Get
        Set(value As Date)
            _fecha_enviado = value
        End Set
    End Property

    Public Property Descuento As Double
        Get
            Return _descuento
        End Get
        Set(value As Double)
            _descuento = value
        End Set
    End Property

    Public Property Area As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property

    Public Property Terminado As String
        Get
            Return _terminado
        End Get
        Set(value As String)
            _terminado = value
        End Set
    End Property

    Public Property Usuario_autorizo As String
        Get
            Return _usuario_autorizo
        End Get
        Set(value As String)
            _usuario_autorizo = value
        End Set
    End Property

    Public Property Surte As String
        Get
            Return _surte
        End Get
        Set(value As String)
            _surte = value
        End Set
    End Property

    Public Property Plazo1 As Integer
        Get
            Return _plazo1
        End Get
        Set(value As Integer)
            _plazo1 = value
        End Set
    End Property

    Public Property Plazo2 As Integer
        Get
            Return _plazo2
        End Get
        Set(value As Integer)
            _plazo2 = value
        End Set
    End Property

    Public Property Plazo3 As Integer
        Get
            Return _plazo3
        End Get
        Set(value As Integer)
            _plazo3 = value
        End Set
    End Property

    Public Property Plazo4 As Integer
        Get
            Return _plazo4
        End Get
        Set(value As Integer)
            _plazo4 = value
        End Set
    End Property

    Public Property Porc1 As Double
        Get
            Return _porc1
        End Get
        Set(value As Double)
            _porc1 = value
        End Set
    End Property

    Public Property Porc2 As Double
        Get
            Return _porc2
        End Get
        Set(value As Double)
            _porc2 = value
        End Set
    End Property

    Public Property Porc3 As Double
        Get
            Return _porc3
        End Get
        Set(value As Double)
            _porc3 = value
        End Set
    End Property

    Public Property Porc4 As Double
        Get
            Return _porc4
        End Get
        Set(value As Double)
            _porc4 = value
        End Set
    End Property

    Public Property Aseguradora As String
        Get
            Return _aseguradora
        End Get
        Set(value As String)
            _aseguradora = value
        End Set
    End Property

    Public Property Viaembarque As String
        Get
            Return _viaembarque
        End Get
        Set(value As String)
            _viaembarque = value
        End Set
    End Property

    Public Property Diasinvant As Integer
        Get
            Return _diasinvant
        End Get
        Set(value As Integer)
            _diasinvant = value
        End Set
    End Property

    Public Property Diasinvnvo As Integer
        Get
            Return _diasinvnvo
        End Get
        Set(value As Integer)
            _diasinvnvo = value
        End Set
    End Property

    Public Property Detalle As List(Of PedidosDet)
        Get
            Return _Detalle
        End Get
        Set(value As List(Of PedidosDet))
            _Detalle = value
        End Set
    End Property

    Public Property OProveedor As Proveedores
        Get
            Return _oProveedor
        End Get
        Set(value As Proveedores)
            _oProveedor = value
        End Set
    End Property

    Public Property Nomcomp As String
        Get
            Return _nomcomp
        End Get
        Set(value As String)
            _nomcomp = value
        End Set
    End Property

    Public Property Comprador_nombre As String
        Get
            Return _comprador_nombre
        End Get
        Set(value As String)
            _comprador_nombre = value
        End Set
    End Property

    Public Property Comprador_clave As String
        Get
            Return _comprador_clave
        End Get
        Set(value As String)
            _comprador_clave = value
        End Set
    End Property

    Public Property Comprador_correo As String
        Get
            Return _comprador_correo
        End Get
        Set(value As String)
            _comprador_correo = value
        End Set
    End Property

    Public Property Comprador_contacto As String
        Get
            Return _comprador_contacto
        End Get
        Set(value As String)
            _comprador_contacto = value
        End Set
    End Property

    Public Property Suc1_direccion As String
        Get
            Return _suc1_direccion
        End Get
        Set(value As String)
            _suc1_direccion = value
        End Set
    End Property

    Public Property Suc1_ciudad1 As String
        Get
            Return suc1_ciudad
        End Get
        Set(value As String)
            suc1_ciudad = value
        End Set
    End Property

    Public Property Suc_entrega_direccion As String
        Get
            Return _suc_entrega_direccion
        End Get
        Set(value As String)
            _suc_entrega_direccion = value
        End Set
    End Property

    Public Property Suc_entrega_ciudad As String
        Get
            Return _suc_entrega_ciudad
        End Get
        Set(value As String)
            _suc_entrega_ciudad = value
        End Set
    End Property
#End Region

    Public Sub Fill(ByVal dtProv As DataTable, ByVal dtPed As DataTable, ByVal dtDet As DataTable)
        Dim oDet As PedidosDet
        Try
            Try
                Me.Id_empresa = dtPed.Rows(0).Item("id_empresa")
            Catch
                Me.Id_empresa = ""
            End Try

            Try
                Me.Id_sucursal = dtPed.Rows(0).Item("id_sucursal")
            Catch
                Me.Id_sucursal = ""
            End Try

            Try
                Me.Pedido = dtPed.Rows(0).Item("pedido")
            Catch
                Me.Pedido = ""
            End Try

            Try
                Me.Tipo = dtPed.Rows(0).Item("tipo")
            Catch
                Me.Tipo = ""
            End Try

            Try
                Me.Afectacion = dtPed.Rows(0).Item("afectacion")
            Catch
                Me.Afectacion = ""
            End Try

            Try
                Me.Fecha = dtPed.Rows(0).Item("fecha")
            Catch
                Me.Fecha = CDate("01/01/1900")
            End Try

            Try
                Me.Proveedor = dtPed.Rows(0).Item("proveedor")
            Catch
                Me.Proveedor = 0
            End Try

            Try
                Me.Depto = dtPed.Rows(0).Item("depto")
            Catch
                Me.Depto = ""
            End Try

            Try
                Me.Estatus = dtPed.Rows(0).Item("estatus")
            Catch
                Me.Estatus = ""
            End Try

            Try
                Me.Pendiente = dtPed.Rows(0).Item("pendiente")
            Catch
                Me.Pendiente = ""
            End Try

            Try
                Me.Id_comprador = dtPed.Rows(0).Item("id_comprador")
            Catch
                Me.Id_comprador = ""
            End Try

            Try
                Me.Condicion = dtPed.Rows(0).Item("condicion")
            Catch
                Me.Condicion = 0
            End Try

            Try
                Me.Prontopago = dtPed.Rows(0).Item("prontopago")
            Catch
                Me.Prontopago = 0.0
            End Try

            Try
                Me.Diasppp = dtPed.Rows(0).Item("diasppp")
            Catch
                Me.Diasppp = 0
            End Try

            Try
                Me.Plazo = dtPed.Rows(0).Item("plazo")
            Catch
                Me.Plazo = 0
            End Try

            Try
                Me.Flete = dtPed.Rows(0).Item("flete")
            Catch
                Me.Flete = 0.0
            End Try

            Try
                Me.Devolucion = dtPed.Rows(0).Item("devolucion")
            Catch
                Me.Devolucion = ""
            End Try

            Try
                Me.Diapago = dtPed.Rows(0).Item("diapago")
            Catch
                Me.Diapago = ""
            End Try

            Try
                Me.Entregas = dtPed.Rows(0).Item("entregas")
            Catch
                Me.Entregas = ""
            End Try

            Try
                Me.Fechabase = dtPed.Rows(0).Item("fechabase")
            Catch
                Me.Fechabase = ""
            End Try

            Try
                Me.Confirmo = dtPed.Rows(0).Item("confirmo")
            Catch
                Me.Confirmo = ""
            End Try

            Try
                Me.Feccap = dtPed.Rows(0).Item("feccap")
            Catch
                Me.Feccap = CDate("01/01/1900")
            End Try

            Try
                Me.Fecimpr = dtPed.Rows(0).Item("fecimpr")
            Catch
                Me.Fecimpr = CDate("01/01/1900")
            End Try

            Try
                Me.Fecentrega = dtPed.Rows(0).Item("fecentrega")
            Catch
                Me.Fecentrega = CDate("01/01/1900")
            End Try

            Try
                Me.Feccancela = dtPed.Rows(0).Item("feccancela")
            Catch
                Me.Feccancela = CDate("01/01/1900")
            End Try

            Try
                Me.Fecrecibo1 = dtPed.Rows(0).Item("fecrecibo1")
            Catch
                Me.Fecrecibo1 = CDate("01/01/1900")
            End Try

            Try
                Me.Fecrecibo2 = dtPed.Rows(0).Item("fecrecibo2")
            Catch
                Me.Fecrecibo2 = CDate("01/01/1900")
            End Try

            Try
                Me.Grupocostos = dtPed.Rows(0).Item("grupocostos")
            Catch
                Me.Grupocostos = ""
            End Try

            Try
                Me.Renglones = dtPed.Rows(0).Item("renglones")
            Catch
                Me.Renglones = 0
            End Try

            Try
                Me.Articulos = dtPed.Rows(0).Item("articulos")
            Catch
                Me.Articulos = 0
            End Try

            Try
                Me.Cantidad = dtPed.Rows(0).Item("cantidad")
            Catch
                Me.Cantidad = 0.0
            End Try

            Try
                Me.Neto = dtPed.Rows(0).Item("neto")
            Catch
                Me.Neto = 0.0
            End Try

            Try
                Me.Costo = dtPed.Rows(0).Item("costo")
            Catch
                Me.Costo = 0.0
            End Try

            Try
                Me.Venta = dtPed.Rows(0).Item("venta")
            Catch
                Me.Venta = 0.0
            End Try

            Try
                Me.Oferta = dtPed.Rows(0).Item("oferta")
            Catch
                Me.Oferta = 0.0
            End Try

            Try
                Me.Impiva = dtPed.Rows(0).Item("impiva")
            Catch
                Me.Impiva = 0.0
            End Try

            Try
                Me.Impivaofe = dtPed.Rows(0).Item("impivaofe")
            Catch
                Me.Impivaofe = 0.0
            End Try

            Try
                Me.Margenvta = dtPed.Rows(0).Item("margenvta")
            Catch
                Me.Margenvta = 0.0
            End Try

            Try
                Me.Margenofe = dtPed.Rows(0).Item("margenofe")
            Catch
                Me.Margenofe = 0.0
            End Try

            Try
                Me.Recarticulos = dtPed.Rows(0).Item("recarticulos")
            Catch
                Me.Recarticulos = 0
            End Try

            Try
                Me.Reccantidad = dtPed.Rows(0).Item("reccantidad")
            Catch
                Me.Reccantidad = 0
            End Try

            Try
                Me.Recneto = dtPed.Rows(0).Item("recneto")
            Catch
                Me.Recneto = 0.0
            End Try

            Try
                Me.Reccosto = dtPed.Rows(0).Item("reccosto")
            Catch
                Me.Reccosto = 0.0
            End Try

            Try
                Me.Recventa = dtPed.Rows(0).Item("recventa")
            Catch
                Me.Recventa = 0.0
            End Try

            Try
                Me.Recoferta = dtPed.Rows(0).Item("recoferta")
            Catch
                Me.Recoferta = 0.0
            End Try

            Try
                Me.Observ1 = dtPed.Rows(0).Item("observ1")
            Catch
                Me.Observ1 = ""
            End Try

            Try
                Me.Observ2 = dtPed.Rows(0).Item("observ2")
            Catch
                Me.Observ2 = ""
            End Try

            Try
                Me.Formato = dtPed.Rows(0).Item("formato")
            Catch
                Me.Formato = ""
            End Try

            Try
                Me.Fecfront = dtPed.Rows(0).Item("fecfront")
            Catch
                Me.Fecfront = CDate("01/01/1900")
            End Try

            Try
                Me.Tcambio1 = dtPed.Rows(0).Item("tcambio1")
            Catch
                Me.Tcambio1 = 0.0
            End Try

            Try
                Me.Tcambio2 = dtPed.Rows(0).Item("tcambio2")
            Catch
                Me.Tcambio2 = 0.0
            End Try

            Try
                Me.Autonumsuc = dtPed.Rows(0).Item("autonumsuc")
            Catch
                Me.Autonumsuc = ""
            End Try

            Try
                Me.Fum = dtPed.Rows(0).Item("fum")
            Catch
                Me.Fum = CDate("01/01/1900")
            End Try

            Try
                Me.Id_usuario = dtPed.Rows(0).Item("id_usuario")
            Catch
                Me.Id_usuario = ""
            End Try

            Try
                Me.Flujo = dtPed.Rows(0).Item("flujo")
            Catch
                Me.Flujo = ""
            End Try

            Try
                Me.Moneda = dtPed.Rows(0).Item("moneda")
            Catch
                Me.Moneda = ""
            End Try

            Try
                Me.Enviado = dtPed.Rows(0).Item("enviado")
            Catch
                Me.Enviado = 1
            End Try

            Try
                Me.Fecha_enviado = dtPed.Rows(0).Item("fecha_enviado")
            Catch
                Me.Fecha_enviado = CDate("01/01/1900")
            End Try

            Try
                Me.Descuento = dtPed.Rows(0).Item("descuento")
            Catch
                Me.Descuento = 0.0
            End Try

            Try
                Me.Area = dtPed.Rows(0).Item("area")
            Catch
                Me.Area = ""
            End Try

            Try
                Me.Terminado = dtPed.Rows(0).Item("terminado")
            Catch
                Me.Terminado = ""
            End Try

            Try
                Me.Usuario_autorizo = dtPed.Rows(0).Item("usuario_autorizo")
            Catch
                Me.Usuario_autorizo = ""
            End Try

            Try
                Me.Surte = dtPed.Rows(0).Item("surte")
            Catch
                Me.Surte = ""
            End Try

            Try
                Me.Plazo1 = dtPed.Rows(0).Item("plazo1")
            Catch
                Me.Plazo1 = 0
            End Try

            Try
                Me.Plazo2 = dtPed.Rows(0).Item("plazo2")
            Catch
                Me.Plazo2 = 0
            End Try

            Try
                Me.Plazo3 = dtPed.Rows(0).Item("plazo3")
            Catch
                Me.Plazo3 = 0
            End Try

            Try
                Me.Plazo4 = dtPed.Rows(0).Item("plazo4")
            Catch
                Me.Plazo4 = 0
            End Try

            Try
                Me.Porc1 = dtPed.Rows(0).Item("porc1")
            Catch
                Me.Porc1 = 0.0
            End Try

            Try
                Me.Porc2 = dtPed.Rows(0).Item("porc2")
            Catch
                Me.Porc2 = 0.0
            End Try

            Try
                Me.Porc3 = dtPed.Rows(0).Item("porc3")
            Catch
                Me.Porc3 = 0.0
            End Try

            Try
                Me.Porc4 = dtPed.Rows(0).Item("porc4")
            Catch
                Me.Porc4 = 0.0
            End Try

            Try
                Me.Aseguradora = dtPed.Rows(0).Item("aseguradora")
            Catch
                Me.Aseguradora = ""
            End Try

            Try
                Me.Viaembarque = dtPed.Rows(0).Item("viaembarque")
            Catch
                Me.Viaembarque = ""
            End Try

            Try
                Me.Diasinvant = dtPed.Rows(0).Item("diasinvant")
            Catch
                Me.Diasinvant = 0
            End Try

            Try
                Me.Diasinvnvo = dtPed.Rows(0).Item("diasinvnvo")
            Catch
                Me.Diasinvnvo = 0
            End Try

            Try
                Me.OProveedor = New Proveedores
                If dtProv.Rows.Count > 0 Then
                    Me.OProveedor.Fill(dtProv.Rows(0))
                End If
            Catch
                Me.OProveedor = Nothing
            End Try

            Try
                Nomcomp = dtPed.Rows(0).Item("Nomcomp")
            Catch
                Nomcomp = ""
            End Try
            Try
                Comprador_nombre = dtPed.Rows(0).Item("Comprador_nombre")
            Catch
                Comprador_nombre = ""
            End Try
            Try
                Comprador_clave = dtPed.Rows(0).Item("Comprador_clave")
            Catch
                Comprador_clave = ""
            End Try
            Try
                Comprador_correo = dtPed.Rows(0).Item("Comprador_correo")
            Catch
                Comprador_correo = ""
            End Try
            Try
                Comprador_contacto = dtPed.Rows(0).Item("Comprador_contacto")
            Catch
                Comprador_contacto = ""
            End Try
            Try
                Suc1_direccion = dtPed.Rows(0).Item("Suc1_direccion")
            Catch
                Suc1_direccion = ""
            End Try
            Try
                suc1_ciudad = dtPed.Rows(0).Item("suc1_ciudad")
            Catch
                suc1_ciudad = ""
            End Try
            Try
                Suc_entrega_direccion = dtPed.Rows(0).Item("Suc_entrega_direccion")
            Catch
                Suc_entrega_direccion = ""
            End Try
            Try
                Suc_entrega_ciudad = dtPed.Rows(0).Item("Suc_entrega_ciudad")
            Catch
                Suc_entrega_ciudad = ""
            End Try

            _Detalle = New List(Of PedidosDet)
            For Each oRen As DataRow In dtDet.Rows
                oDet = New PedidosDet
                oDet.Fill(oRen)
                _Detalle.Add(oDet)
            Next

        Catch ex As Exception

        End Try
    End Sub
End Class
Public Class PedidosDet
    Dim _id_empresa As String
    Dim _id_sucursal As String
    Dim _pedido As String
    Dim _articulo As Integer
    Dim _id_sucursal1 As String
    Dim _ped_pedido As String
    Dim _cve_sucursal1 As String
    Dim _renglon As Integer
    Dim _costo As Double
    Dim _costoneto As Double
    Dim _venta As Double
    Dim _oferta As Double
    Dim _margen As Double
    Dim _iva As String
    Dim _impiva As Double
    Dim _impivaofe As Double
    Dim _pedida As Decimal
    Dim _recibida As Decimal
    Dim _danada As Decimal
    Dim _cantpendiente As Decimal
    Dim _tyc As String
    Dim _condicion As Integer
    Dim _descto1 As Double
    Dim _descto2 As Double
    Dim _descto3 As Double
    Dim _descto4 As Double
    Dim _descto5 As Double
    Dim _desctoadic1 As Double
    Dim _desctoadic2 As Double
    Dim _desctoadic3 As Double
    Dim _prontopago As Double
    Dim _factor As Decimal
    Dim _estatus As String
    Dim _autonumsuc As String
    Dim _fum As Date
    Dim _id_usuario As String
    Dim _contiene As Decimal
    Dim _comentario As String
    Dim _usuario_solicito As String
    Dim _id_ccosto As String
    Dim _nombre As String
    Dim _codigo As String
    Dim _codigoref As String
    Dim _unidadvta As String

#Region "PROPIEDADES"
    Public Property Id_empresa As String
        Get
            Return _id_empresa
        End Get
        Set(value As String)
            _id_empresa = value
        End Set
    End Property

    Public Property Id_sucursal As String
        Get
            Return _id_sucursal
        End Get
        Set(value As String)
            _id_sucursal = value
        End Set
    End Property

    Public Property Pedido As String
        Get
            Return _pedido
        End Get
        Set(value As String)
            _pedido = value
        End Set
    End Property

    Public Property Articulo As Integer
        Get
            Return _articulo
        End Get
        Set(value As Integer)
            _articulo = value
        End Set
    End Property

    Public Property Id_sucursal1 As String
        Get
            Return _id_sucursal1
        End Get
        Set(value As String)
            _id_sucursal1 = value
        End Set
    End Property

    Public Property Ped_pedido As String
        Get
            Return _ped_pedido
        End Get
        Set(value As String)
            _ped_pedido = value
        End Set
    End Property

    Public Property Cve_sucursal1 As String
        Get
            Return _cve_sucursal1
        End Get
        Set(value As String)
            _cve_sucursal1 = value
        End Set
    End Property

    Public Property Renglon As Integer
        Get
            Return _renglon
        End Get
        Set(value As Integer)
            _renglon = value
        End Set
    End Property

    Public Property Costo As Double
        Get
            Return _costo
        End Get
        Set(value As Double)
            _costo = value
        End Set
    End Property

    Public Property Costoneto As Double
        Get
            Return _costoneto
        End Get
        Set(value As Double)
            _costoneto = value
        End Set
    End Property

    Public Property Venta As Double
        Get
            Return _venta
        End Get
        Set(value As Double)
            _venta = value
        End Set
    End Property

    Public Property Oferta As Double
        Get
            Return _oferta
        End Get
        Set(value As Double)
            _oferta = value
        End Set
    End Property

    Public Property Margen As Double
        Get
            Return _margen
        End Get
        Set(value As Double)
            _margen = value
        End Set
    End Property

    Public Property Iva As Double
        Get
            Return _iva
        End Get
        Set(value As Double)
            _iva = value
        End Set
    End Property

    Public Property Impiva As Double
        Get
            Return _impiva
        End Get
        Set(value As Double)
            _impiva = value
        End Set
    End Property

    Public Property Impivaofe As Double
        Get
            Return _impivaofe
        End Get
        Set(value As Double)
            _impivaofe = value
        End Set
    End Property

    Public Property Pedida As Decimal
        Get
            Return _pedida
        End Get
        Set(value As Decimal)
            _pedida = value
        End Set
    End Property

    Public Property Recibida As Decimal
        Get
            Return _recibida
        End Get
        Set(value As Decimal)
            _recibida = value
        End Set
    End Property

    Public Property Danada As Decimal
        Get
            Return _danada
        End Get
        Set(value As Decimal)
            _danada = value
        End Set
    End Property

    Public Property Cantpendiente As Decimal
        Get
            Return _cantpendiente
        End Get
        Set(value As Decimal)
            _cantpendiente = value
        End Set
    End Property

    Public Property Tyc As String
        Get
            Return _tyc
        End Get
        Set(value As String)
            _tyc = value
        End Set
    End Property

    Public Property Condicion As Integer
        Get
            Return _condicion
        End Get
        Set(value As Integer)
            _condicion = value
        End Set
    End Property

    Public Property Descto1 As Double
        Get
            Return _descto1
        End Get
        Set(value As Double)
            _descto1 = value
        End Set
    End Property

    Public Property Descto2 As Double
        Get
            Return _descto2
        End Get
        Set(value As Double)
            _descto2 = value
        End Set
    End Property

    Public Property Descto3 As Double
        Get
            Return _descto3
        End Get
        Set(value As Double)
            _descto3 = value
        End Set
    End Property

    Public Property Descto4 As Double
        Get
            Return _descto4
        End Get
        Set(value As Double)
            _descto4 = value
        End Set
    End Property

    Public Property Descto5 As Double
        Get
            Return _descto5
        End Get
        Set(value As Double)
            _descto5 = value
        End Set
    End Property

    Public Property Desctoadic1 As Double
        Get
            Return _desctoadic1
        End Get
        Set(value As Double)
            _desctoadic1 = value
        End Set
    End Property

    Public Property Desctoadic2 As Double
        Get
            Return _desctoadic2
        End Get
        Set(value As Double)
            _desctoadic2 = value
        End Set
    End Property

    Public Property Desctoadic3 As Double
        Get
            Return _desctoadic3
        End Get
        Set(value As Double)
            _desctoadic3 = value
        End Set
    End Property

    Public Property Prontopago As Double
        Get
            Return _prontopago
        End Get
        Set(value As Double)
            _prontopago = value
        End Set
    End Property

    Public Property Factor As Decimal
        Get
            Return _factor
        End Get
        Set(value As Decimal)
            _factor = value
        End Set
    End Property

    Public Property Estatus As String
        Get
            Return _estatus
        End Get
        Set(value As String)
            _estatus = value
        End Set
    End Property

    Public Property Autonumsuc As String
        Get
            Return _autonumsuc
        End Get
        Set(value As String)
            _autonumsuc = value
        End Set
    End Property

    Public Property Fum As Date
        Get
            Return _fum
        End Get
        Set(value As Date)
            _fum = value
        End Set
    End Property

    Public Property Id_usuario As String
        Get
            Return _id_usuario
        End Get
        Set(value As String)
            _id_usuario = value
        End Set
    End Property

    Public Property Contiene As Decimal
        Get
            Return _contiene
        End Get
        Set(value As Decimal)
            _contiene = value
        End Set
    End Property

    Public Property Comentario As String
        Get
            Return _comentario
        End Get
        Set(value As String)
            _comentario = value
        End Set
    End Property

    Public Property Usuario_solicito As String
        Get
            Return _usuario_solicito
        End Get
        Set(value As String)
            _usuario_solicito = value
        End Set
    End Property

    Public Property Id_ccosto As String
        Get
            Return _id_ccosto
        End Get
        Set(value As String)
            _id_ccosto = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property

    Public Property Codigoref As String
        Get
            Return _codigoref
        End Get
        Set(value As String)
            _codigoref = value
        End Set
    End Property

    Public Property Unidadvta As String
        Get
            Return _unidadvta
        End Get
        Set(value As String)
            _unidadvta = value
        End Set
    End Property
#End Region
    Public Sub Fill(ByVal oRen As DataRow)
        Try
            Try
                Me.Id_empresa = oRen.Item("id_empresa")
            Catch
                Me.Id_empresa = ""
            End Try
            Try
                Me.Id_sucursal = oRen.Item("id_sucursal")
            Catch
                Me.Id_sucursal = ""
            End Try
            Try
                Me.Pedido = oRen.Item("pedido")
            Catch
                Me.Pedido = ""
            End Try
            Try
                Me.Articulo = oRen.Item("articulo")
            Catch
                Me.Articulo = 0
            End Try
            Try
                Me.Id_sucursal1 = oRen.Item("id_sucursal1")
            Catch
                Me.Id_sucursal1 = ""
            End Try
            Try
                Me.Ped_pedido = oRen.Item("ped_pedido")
            Catch
                Me.Ped_pedido = ""
            End Try
            Try
                Me.Cve_sucursal1 = oRen.Item("cve_sucursal1")
            Catch
                Me.Cve_sucursal1 = ""
            End Try
            Try
                Me.Renglon = oRen.Item("renglon")
            Catch
                Me.Renglon = 0
            End Try
            Try
                Me.Costo = oRen.Item("costo")
            Catch
                Me.Costo = 0.0
            End Try
            Try
                Me.Costoneto = oRen.Item("costoneto")
            Catch
                Me.Costoneto = 0.0
            End Try
            Try
                Me.Venta = oRen.Item("venta")
            Catch
                Me.Venta = 0.0
            End Try
            Try
                Me.Oferta = oRen.Item("oferta")
            Catch
                Me.Oferta = 0.0
            End Try
            Try
                Me.Margen = oRen.Item("margen")
            Catch
                Me.Margen = 0.0
            End Try
            Try
                Me.Iva = Double.Parse(oRen.Item("iva")) / 100.0
            Catch
                Me.Iva = 0.0
            End Try
            Try
                Me.Impiva = oRen.Item("impiva")
            Catch
                Me.Impiva = 0.0
            End Try
            Try
                Me.Impivaofe = oRen.Item("impivaofe")
            Catch
                Me.Impivaofe = 0.0
            End Try
            Try
                Me.Pedida = oRen.Item("pedida")
            Catch
                Me.Pedida = 0.0
            End Try
            Try
                Me.Recibida = oRen.Item("recibida")
            Catch
                Me.Recibida = 0.0
            End Try
            Try
                Me.Danada = oRen.Item("danada")
            Catch
                Me.Danada = 0.0
            End Try
            Try
                Me.Cantpendiente = oRen.Item("cantpendiente")
            Catch
                Me.Cantpendiente = 0.0
            End Try
            Try
                Me.Tyc = oRen.Item("tyc")
            Catch
                Me.Tyc = ""
            End Try
            Try
                Me.Condicion = oRen.Item("condicion")
            Catch
                Me.Condicion = 0
            End Try
            Try
                Me.Descto1 = oRen.Item("descto1")
            Catch
                Me.Descto1 = 0.0
            End Try
            Try
                Me.Descto2 = oRen.Item("descto2")
            Catch
                Me.Descto2 = 0.0
            End Try
            Try
                Me.Descto3 = oRen.Item("descto3")
            Catch
                Me.Descto3 = 0.0
            End Try
            Try
                Me.Descto4 = oRen.Item("descto4")
            Catch
                Me.Descto4 = 0.0
            End Try
            Try
                Me.Descto5 = oRen.Item("descto5")
            Catch
                Me.Descto5 = 0.0
            End Try
            Try
                Me.Desctoadic1 = oRen.Item("desctoadic1")
            Catch
                Me.Desctoadic1 = 0.0
            End Try
            Try
                Me.Desctoadic2 = oRen.Item("desctoadic2")
            Catch
                Me.Desctoadic2 = 0.0
            End Try
            Try
                Me.Desctoadic3 = oRen.Item("desctoadic3")
            Catch
                Me.Desctoadic3 = 0.0
            End Try
            Try
                Me.Prontopago = oRen.Item("prontopago")
            Catch
                Me.Prontopago = 0.0
            End Try
            Try
                Me.Factor = oRen.Item("factor")
            Catch
                Me.Factor = 0.0
            End Try
            Try
                Me.Estatus = oRen.Item("estatus")
            Catch
                Me.Estatus = ""
            End Try
            Try
                Me.Autonumsuc = oRen.Item("autonumsuc")
            Catch
                Me.Autonumsuc = ""
            End Try
            Try
                Me.Fum = oRen.Item("fum")
            Catch
                Me.Fum = CDate("01/01/1900")
            End Try
            Try
                Me.Id_usuario = oRen.Item("id_usuario")
            Catch
                Me.Id_usuario = ""
            End Try
            Try
                Me.Contiene = oRen.Item("contiene")
            Catch
                Me.Contiene = 0.0
            End Try
            Try
                Me.Comentario = oRen.Item("comentario")
            Catch
                Me.Comentario = ""
            End Try
            Try
                Me.Usuario_solicito = oRen.Item("usuario_solicito")
            Catch
                Me.Usuario_solicito = ""
            End Try
            Try
                Me.Id_ccosto = oRen.Item("id_ccosto")
            Catch
                Me.Id_ccosto = ""
            End Try
            Try
                Me.Nombre = oRen.Item("nombre")
            Catch
                Me.Nombre = ""
            End Try
            Try
                Me.Codigo = oRen.Item("codigo")
            Catch
                Me.Codigo = ""
            End Try
            Try
                Me.Codigoref = oRen.Item("codigoref")
            Catch
                Me.Codigoref = ""
            End Try
            Try
                Me.Unidadvta = oRen.Item("unidadvta")
            Catch
                Me.Unidadvta = ""
            End Try

        Catch ex As Exception

        End Try
    End Sub
End Class
Public Class Proveedores
    Dim _proveedor As Integer
    Dim _tipo As String
    Dim _estatus As String
    Dim _nombre As String
    Dim _nomcorto As String
    Dim _direccion As String
    Dim _entrecalles As String
    Dim _colonia As String
    Dim _ciudad As String
    Dim _edo As String
    Dim _pais As String
    Dim _cpostal As String
    Dim _telefono As String
    Dim _telefono2 As String
    Dim _email As String
    Dim _internet As String
    Dim _fax As String
    Dim _atencion As String
    Dim _dirfab1 As String
    Dim _dirfab2 As String
    Dim _dirfab3 As String
    Dim _telfab As String
    Dim _faxfab As String
    Dim _atnfab As String
    Dim _dirpag1 As String
    Dim _dirpag2 As String
    Dim _dirpag3 As String
    Dim _telpag As String
    Dim _faxpag As String
    Dim _atnpag As String
    Dim _dirage1 As String
    Dim _dirage2 As String
    Dim _dirage3 As String
    Dim _telage As String
    Dim _faxage As String
    Dim _atnage As String
    Dim _dirdev1 As String
    Dim _dirdev2 As String
    Dim _dirdev3 As String
    Dim _teldev As String
    Dim _faxdev As String
    Dim _atndev As String
    Dim _localiz As String
    Dim _rfc As String
    Dim _regsecofi As String
    Dim _claveaux As String
    Dim _plazo As Integer
    Dim _medioent As String
    Dim _lugarent As String
    Dim _formapago As String
    Dim _id_banco As String
    Dim _ctabanco As String
    Dim _diasresp As Double
    Dim _condicion As Double
    Dim _resurtido As String
    Dim _ctactb1 As String
    Dim _ctactb2 As String
    Dim _fum As Date
    Dim _id_usuario As String
    Dim _num_plaza As String
    Dim _num_sucursal As String
    Dim _referencia As String
    Dim _lugar As String
    Dim _factorresurtido As Double
    Dim _sistema As String
    Dim _estatus_dev As String
    Dim _tipo_prov As String
    Dim _clase As String
    Dim _descripcion_estable As String
    Dim _negociacion As String
    Dim _dias_pedido As String
    Dim _dias_entrega As String
    Dim _persona_autorizada As String
    Dim _persona_autorizada1 As String
    Dim _persona_autorizada2 As String
    Dim _persona_certifica As String
    Dim _agrupar As String
    Dim _pago_dia As String
    Dim _pago_tipo As String
    Dim _entradapropia As String
    Dim _cantprov As String
#Region "Propiedades"

    Public Property Proveedor As Integer
        Get
            Return _proveedor
        End Get
        Set(value As Integer)
            _proveedor = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Estatus As String
        Get
            Return _estatus
        End Get
        Set(value As String)
            _estatus = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Property Nomcorto As String
        Get
            Return _nomcorto
        End Get
        Set(value As String)
            _nomcorto = value
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
        End Set
    End Property

    Public Property Entrecalles As String
        Get
            Return _entrecalles
        End Get
        Set(value As String)
            _entrecalles = value
        End Set
    End Property

    Public Property Colonia As String
        Get
            Return _colonia
        End Get
        Set(value As String)
            _colonia = value
        End Set
    End Property

    Public Property Ciudad As String
        Get
            Return _ciudad
        End Get
        Set(value As String)
            _ciudad = value
        End Set
    End Property

    Public Property Edo As String
        Get
            Return _edo
        End Get
        Set(value As String)
            _edo = value
        End Set
    End Property

    Public Property Pais As String
        Get
            Return _pais
        End Get
        Set(value As String)
            _pais = value
        End Set
    End Property

    Public Property Cpostal As String
        Get
            Return _cpostal
        End Get
        Set(value As String)
            _cpostal = value
        End Set
    End Property

    Public Property Telefono As String
        Get
            Return _telefono
        End Get
        Set(value As String)
            _telefono = value
        End Set
    End Property

    Public Property Telefono2 As String
        Get
            Return _telefono2
        End Get
        Set(value As String)
            _telefono2 = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Internet As String
        Get
            Return _internet
        End Get
        Set(value As String)
            _internet = value
        End Set
    End Property

    Public Property Fax As String
        Get
            Return _fax
        End Get
        Set(value As String)
            _fax = value
        End Set
    End Property

    Public Property Atencion As String
        Get
            Return _atencion
        End Get
        Set(value As String)
            _atencion = value
        End Set
    End Property

    Public Property Dirfab1 As String
        Get
            Return _dirfab1
        End Get
        Set(value As String)
            _dirfab1 = value
        End Set
    End Property

    Public Property Dirfab2 As String
        Get
            Return _dirfab2
        End Get
        Set(value As String)
            _dirfab2 = value
        End Set
    End Property

    Public Property Dirfab3 As String
        Get
            Return _dirfab3
        End Get
        Set(value As String)
            _dirfab3 = value
        End Set
    End Property

    Public Property Telfab As String
        Get
            Return _telfab
        End Get
        Set(value As String)
            _telfab = value
        End Set
    End Property

    Public Property Faxfab As String
        Get
            Return _faxfab
        End Get
        Set(value As String)
            _faxfab = value
        End Set
    End Property

    Public Property Atnfab As String
        Get
            Return _atnfab
        End Get
        Set(value As String)
            _atnfab = value
        End Set
    End Property

    Public Property Dirpag1 As String
        Get
            Return _dirpag1
        End Get
        Set(value As String)
            _dirpag1 = value
        End Set
    End Property

    Public Property Dirpag2 As String
        Get
            Return _dirpag2
        End Get
        Set(value As String)
            _dirpag2 = value
        End Set
    End Property

    Public Property Dirpag3 As String
        Get
            Return _dirpag3
        End Get
        Set(value As String)
            _dirpag3 = value
        End Set
    End Property

    Public Property Telpag As String
        Get
            Return _telpag
        End Get
        Set(value As String)
            _telpag = value
        End Set
    End Property

    Public Property Faxpag As String
        Get
            Return _faxpag
        End Get
        Set(value As String)
            _faxpag = value
        End Set
    End Property

    Public Property Atnpag As String
        Get
            Return _atnpag
        End Get
        Set(value As String)
            _atnpag = value
        End Set
    End Property

    Public Property Dirage1 As String
        Get
            Return _dirage1
        End Get
        Set(value As String)
            _dirage1 = value
        End Set
    End Property

    Public Property Dirage2 As String
        Get
            Return _dirage2
        End Get
        Set(value As String)
            _dirage2 = value
        End Set
    End Property

    Public Property Dirage3 As String
        Get
            Return _dirage3
        End Get
        Set(value As String)
            _dirage3 = value
        End Set
    End Property

    Public Property Telage As String
        Get
            Return _telage
        End Get
        Set(value As String)
            _telage = value
        End Set
    End Property

    Public Property Faxage As String
        Get
            Return _faxage
        End Get
        Set(value As String)
            _faxage = value
        End Set
    End Property

    Public Property Atnage As String
        Get
            Return _atnage
        End Get
        Set(value As String)
            _atnage = value
        End Set
    End Property

    Public Property Dirdev1 As String
        Get
            Return _dirdev1
        End Get
        Set(value As String)
            _dirdev1 = value
        End Set
    End Property

    Public Property Dirdev2 As String
        Get
            Return _dirdev2
        End Get
        Set(value As String)
            _dirdev2 = value
        End Set
    End Property

    Public Property Dirdev3 As String
        Get
            Return _dirdev3
        End Get
        Set(value As String)
            _dirdev3 = value
        End Set
    End Property

    Public Property Teldev As String
        Get
            Return _teldev
        End Get
        Set(value As String)
            _teldev = value
        End Set
    End Property

    Public Property Faxdev As String
        Get
            Return _faxdev
        End Get
        Set(value As String)
            _faxdev = value
        End Set
    End Property

    Public Property Atndev As String
        Get
            Return _atndev
        End Get
        Set(value As String)
            _atndev = value
        End Set
    End Property

    Public Property Localiz As String
        Get
            Return _localiz
        End Get
        Set(value As String)
            _localiz = value
        End Set
    End Property

    Public Property Rfc As String
        Get
            Return _rfc
        End Get
        Set(value As String)
            _rfc = value
        End Set
    End Property

    Public Property Regsecofi As String
        Get
            Return _regsecofi
        End Get
        Set(value As String)
            _regsecofi = value
        End Set
    End Property

    Public Property Claveaux As String
        Get
            Return _claveaux
        End Get
        Set(value As String)
            _claveaux = value
        End Set
    End Property

    Public Property Plazo As Integer
        Get
            Return _plazo
        End Get
        Set(value As Integer)
            _plazo = value
        End Set
    End Property

    Public Property Medioent As String
        Get
            Return _medioent
        End Get
        Set(value As String)
            _medioent = value
        End Set
    End Property

    Public Property Lugarent As String
        Get
            Return _lugarent
        End Get
        Set(value As String)
            _lugarent = value
        End Set
    End Property

    Public Property Formapago As String
        Get
            Return _formapago
        End Get
        Set(value As String)
            _formapago = value
        End Set
    End Property

    Public Property Id_banco As String
        Get
            Return _id_banco
        End Get
        Set(value As String)
            _id_banco = value
        End Set
    End Property

    Public Property Ctabanco As String
        Get
            Return _ctabanco
        End Get
        Set(value As String)
            _ctabanco = value
        End Set
    End Property

    Public Property Diasresp As Double
        Get
            Return _diasresp
        End Get
        Set(value As Double)
            _diasresp = value
        End Set
    End Property

    Public Property Condicion As Double
        Get
            Return _condicion
        End Get
        Set(value As Double)
            _condicion = value
        End Set
    End Property

    Public Property Resurtido As String
        Get
            Return _resurtido
        End Get
        Set(value As String)
            _resurtido = value
        End Set
    End Property

    Public Property Ctactb1 As String
        Get
            Return _ctactb1
        End Get
        Set(value As String)
            _ctactb1 = value
        End Set
    End Property

    Public Property Ctactb2 As String
        Get
            Return _ctactb2
        End Get
        Set(value As String)
            _ctactb2 = value
        End Set
    End Property

    Public Property Fum As Date
        Get
            Return _fum
        End Get
        Set(value As Date)
            _fum = value
        End Set
    End Property

    Public Property Id_usuario As String
        Get
            Return _id_usuario
        End Get
        Set(value As String)
            _id_usuario = value
        End Set
    End Property

    Public Property Num_plaza As String
        Get
            Return _num_plaza
        End Get
        Set(value As String)
            _num_plaza = value
        End Set
    End Property

    Public Property Num_sucursal As String
        Get
            Return _num_sucursal
        End Get
        Set(value As String)
            _num_sucursal = value
        End Set
    End Property

    Public Property Referencia As String
        Get
            Return _referencia
        End Get
        Set(value As String)
            _referencia = value
        End Set
    End Property

    Public Property Lugar As String
        Get
            Return _lugar
        End Get
        Set(value As String)
            _lugar = value
        End Set
    End Property

    Public Property Factorresurtido As Double
        Get
            Return _factorresurtido
        End Get
        Set(value As Double)
            _factorresurtido = value
        End Set
    End Property

    Public Property Sistema As String
        Get
            Return _sistema
        End Get
        Set(value As String)
            _sistema = value
        End Set
    End Property

    Public Property Estatus_dev As String
        Get
            Return _estatus_dev
        End Get
        Set(value As String)
            _estatus_dev = value
        End Set
    End Property

    Public Property Tipo_prov As String
        Get
            Return _tipo_prov
        End Get
        Set(value As String)
            _tipo_prov = value
        End Set
    End Property

    Public Property Clase As String
        Get
            Return _clase
        End Get
        Set(value As String)
            _clase = value
        End Set
    End Property

    Public Property Descripcion_estable As String
        Get
            Return _descripcion_estable
        End Get
        Set(value As String)
            _descripcion_estable = value
        End Set
    End Property

    Public Property Negociacion As String
        Get
            Return _negociacion
        End Get
        Set(value As String)
            _negociacion = value
        End Set
    End Property

    Public Property Dias_pedido As String
        Get
            Return _dias_pedido
        End Get
        Set(value As String)
            _dias_pedido = value
        End Set
    End Property

    Public Property Dias_entrega As String
        Get
            Return _dias_entrega
        End Get
        Set(value As String)
            _dias_entrega = value
        End Set
    End Property

    Public Property Persona_autorizada As String
        Get
            Return _persona_autorizada
        End Get
        Set(value As String)
            _persona_autorizada = value
        End Set
    End Property

    Public Property Persona_autorizada1 As String
        Get
            Return _persona_autorizada1
        End Get
        Set(value As String)
            _persona_autorizada1 = value
        End Set
    End Property

    Public Property Persona_autorizada2 As String
        Get
            Return _persona_autorizada2
        End Get
        Set(value As String)
            _persona_autorizada2 = value
        End Set
    End Property

    Public Property Persona_certifica As String
        Get
            Return _persona_certifica
        End Get
        Set(value As String)
            _persona_certifica = value
        End Set
    End Property

    Public Property Agrupar As String
        Get
            Return _agrupar
        End Get
        Set(value As String)
            _agrupar = value
        End Set
    End Property

    Public Property Pago_dia As String
        Get
            Return _pago_dia
        End Get
        Set(value As String)
            _pago_dia = value
        End Set
    End Property

    Public Property Pago_tipo As String
        Get
            Return _pago_tipo
        End Get
        Set(value As String)
            _pago_tipo = value
        End Set
    End Property

    Public Property Entradapropia As String
        Get
            Return _entradapropia
        End Get
        Set(value As String)
            _entradapropia = value
        End Set
    End Property

    Public Property Cantprov As String
        Get
            Return _cantprov
        End Get
        Set(value As String)
            _cantprov = value
        End Set
    End Property

#End Region
    Public Sub Fill(ByVal oRen As DataRow)
        Try
            Me.Proveedor = oRen.Item("proveedor")
        Catch
            Me.Proveedor = 0
        End Try
        Try
            Me.Tipo = oRen.Item("tipo")
        Catch
            Me.Tipo = ""
        End Try
        Try
            Me.Estatus = oRen.Item("estatus")
        Catch
            Me.Estatus = ""
        End Try
        Try
            Me.Nombre = oRen.Item("nombre")
        Catch
            Me.Nombre = ""
        End Try
        Try
            Me.Nomcorto = oRen.Item("nomcorto")
        Catch
            Me.Nomcorto = ""
        End Try
        Try
            Me.Direccion = oRen.Item("direccion")
        Catch
            Me.Direccion = ""
        End Try
        Try
            Me.Entrecalles = oRen.Item("entrecalles")
        Catch
            Me.Entrecalles = ""
        End Try
        Try
            Me.Colonia = oRen.Item("colonia")
        Catch
            Me.Colonia = ""
        End Try
        Try
            Me.Ciudad = oRen.Item("ciudad")
        Catch
            Me.Ciudad = ""
        End Try
        Try
            Me.Edo = oRen.Item("edo")
        Catch
            Me.Edo = ""
        End Try
        Try
            Me.Pais = oRen.Item("pais")
        Catch
            Me.Pais = ""
        End Try
        Try
            Me.Cpostal = oRen.Item("cpostal")
        Catch
            Me.Cpostal = ""
        End Try
        Try
            Me.Telefono = oRen.Item("telefono")
        Catch
            Me.Telefono = ""
        End Try
        Try
            Me.Telefono2 = oRen.Item("telefono2")
        Catch
            Me.Telefono2 = ""
        End Try
        Try
            Me.Email = oRen.Item("email")
        Catch
            Me.Email = ""
        End Try
        Try
            Me.Internet = oRen.Item("internet")
        Catch
            Me.Internet = ""
        End Try
        Try
            Me.Fax = oRen.Item("fax")
        Catch
            Me.Fax = ""
        End Try
        Try
            Me.Atencion = oRen.Item("atencion")
        Catch
            Me.Atencion = ""
        End Try
        Try
            Me.Dirfab1 = oRen.Item("dirfab1")
        Catch
            Me.Dirfab1 = ""
        End Try
        Try
            Me.Dirfab2 = oRen.Item("dirfab2")
        Catch
            Me.Dirfab2 = ""
        End Try
        Try
            Me.Dirfab3 = oRen.Item("dirfab3")
        Catch
            Me.Dirfab3 = ""
        End Try
        Try
            Me.Telfab = oRen.Item("telfab")
        Catch
            Me.Telfab = ""
        End Try
        Try
            Me.Faxfab = oRen.Item("faxfab")
        Catch
            Me.Faxfab = ""
        End Try
        Try
            Me.Atnfab = oRen.Item("atnfab")
        Catch
            Me.Atnfab = ""
        End Try
        Try
            Me.Dirpag1 = oRen.Item("dirpag1")
        Catch
            Me.Dirpag1 = ""
        End Try
        Try
            Me.Dirpag2 = oRen.Item("dirpag2")
        Catch
            Me.Dirpag2 = ""
        End Try
        Try
            Me.Dirpag3 = oRen.Item("dirpag3")
        Catch
            Me.Dirpag3 = ""
        End Try
        Try
            Me.Telpag = oRen.Item("telpag")
        Catch
            Me.Telpag = ""
        End Try
        Try
            Me.Faxpag = oRen.Item("faxpag")
        Catch
            Me.Faxpag = ""
        End Try
        Try
            Me.Atnpag = oRen.Item("atnpag")
        Catch
            Me.Atnpag = ""
        End Try
        Try
            Me.Dirage1 = oRen.Item("dirage1")
        Catch
            Me.Dirage1 = ""
        End Try
        Try
            Me.Dirage2 = oRen.Item("dirage2")
        Catch
            Me.Dirage2 = ""
        End Try
        Try
            Me.Dirage3 = oRen.Item("dirage3")
        Catch
            Me.Dirage3 = ""
        End Try
        Try
            Me.Telage = oRen.Item("telage")
        Catch
            Me.Telage = ""
        End Try
        Try
            Me.Faxage = oRen.Item("faxage")
        Catch
            Me.Faxage = ""
        End Try
        Try
            Me.Atnage = oRen.Item("atnage")
        Catch
            Me.Atnage = ""
        End Try
        Try
            Me.Dirdev1 = oRen.Item("dirdev1")
        Catch
            Me.Dirdev1 = ""
        End Try
        Try
            Me.Dirdev2 = oRen.Item("dirdev2")
        Catch
            Me.Dirdev2 = ""
        End Try
        Try
            Me.Dirdev3 = oRen.Item("dirdev3")
        Catch
            Me.Dirdev3 = ""
        End Try
        Try
            Me.Teldev = oRen.Item("teldev")
        Catch
            Me.Teldev = ""
        End Try
        Try
            Me.Faxdev = oRen.Item("faxdev")
        Catch
            Me.Faxdev = ""
        End Try
        Try
            Me.Atndev = oRen.Item("atndev")
        Catch
            Me.Atndev = ""
        End Try
        Try
            Me.Localiz = oRen.Item("localiz")
        Catch
            Me.Localiz = ""
        End Try
        Try
            Me.Rfc = oRen.Item("rfc")
        Catch
            Me.Rfc = ""
        End Try
        Try
            Me.Regsecofi = oRen.Item("regsecofi")
        Catch
            Me.Regsecofi = ""
        End Try
        Try
            Me.Claveaux = oRen.Item("claveaux")
        Catch
            Me.Claveaux = ""
        End Try
        Try
            Me.Plazo = oRen.Item("plazo")
        Catch
            Me.Plazo = 0
        End Try
        Try
            Me.Medioent = oRen.Item("medioent")
        Catch
            Me.Medioent = ""
        End Try
        Try
            Me.Lugarent = oRen.Item("lugarent")
        Catch
            Me.Lugarent = ""
        End Try
        Try
            Me.Formapago = oRen.Item("formapago")
        Catch
            Me.Formapago = ""
        End Try
        Try
            Me.Id_banco = oRen.Item("id_banco")
        Catch
            Me.Id_banco = ""
        End Try
        Try
            Me.Ctabanco = oRen.Item("ctabanco")
        Catch
            Me.Ctabanco = ""
        End Try
        Try
            Me.Diasresp = oRen.Item("diasresp")
        Catch
            Me.Diasresp = 0.0
        End Try
        Try
            Me.Condicion = oRen.Item("condicion")
        Catch
            Me.Condicion = 0.0
        End Try
        Try
            Me.Resurtido = oRen.Item("resurtido")
        Catch
            Me.Resurtido = ""
        End Try
        Try
            Me.Ctactb1 = oRen.Item("ctactb1")
        Catch
            Me.Ctactb1 = ""
        End Try
        Try
            Me.Ctactb2 = oRen.Item("ctactb2")
        Catch
            Me.Ctactb2 = ""
        End Try
        Try
            Me.Fum = oRen.Item("fum")
        Catch
            Me.Fum = CDate("01/01/1900")
        End Try
        Try
            Me.Id_usuario = oRen.Item("id_usuario")
        Catch
            Me.Id_usuario = ""
        End Try
        Try
            Me.Num_plaza = oRen.Item("num_plaza")
        Catch
            Me.Num_plaza = ""
        End Try
        Try
            Me.Num_sucursal = oRen.Item("num_sucursal")
        Catch
            Me.Num_sucursal = ""
        End Try
        Try
            Me.Referencia = oRen.Item("referencia")
        Catch
            Me.Referencia = ""
        End Try
        Try
            Me.Lugar = oRen.Item("lugar")
        Catch
            Me.Lugar = ""
        End Try
        Try
            Me.Factorresurtido = oRen.Item("factorresurtido")
        Catch
            Me.Factorresurtido = 0.0
        End Try
        Try
            Me.Sistema = oRen.Item("sistema")
        Catch
            Me.Sistema = ""
        End Try
        Try
            Me.Estatus_dev = oRen.Item("estatus_dev")
        Catch
            Me.Estatus_dev = ""
        End Try
        Try
            Me.Tipo_prov = oRen.Item("tipo_prov")
        Catch
            Me.Tipo_prov = ""
        End Try
        Try
            Me.Clase = oRen.Item("clase")
        Catch
            Me.Clase = ""
        End Try
        Try
            Me.Descripcion_estable = oRen.Item("descripcion_estable")
        Catch
            Me.Descripcion_estable = ""
        End Try
        Try
            Me.Negociacion = oRen.Item("negociacion")
        Catch
            Me.Negociacion = ""
        End Try
        Try
            Me.Dias_pedido = oRen.Item("dias_pedido")
        Catch
            Me.Dias_pedido = ""
        End Try
        Try
            Me.Dias_entrega = oRen.Item("dias_entrega")
        Catch
            Me.Dias_entrega = ""
        End Try
        Try
            Me.Persona_autorizada = oRen.Item("persona_autorizada")
        Catch
            Me.Persona_autorizada = ""
        End Try
        Try
            Me.Persona_autorizada1 = oRen.Item("persona_autorizada1")
        Catch
            Me.Persona_autorizada1 = ""
        End Try
        Try
            Me.Persona_autorizada2 = oRen.Item("persona_autorizada2")
        Catch
            Me.Persona_autorizada2 = ""
        End Try
        Try
            Me.Persona_certifica = oRen.Item("persona_certifica")
        Catch
            Me.Persona_certifica = ""
        End Try
        Try
            Me.Agrupar = oRen.Item("agrupar")
        Catch
            Me.Agrupar = ""
        End Try
        Try
            Me.Pago_dia = oRen.Item("pago_dia")
        Catch
            Me.Pago_dia = ""
        End Try
        Try
            Me.Pago_tipo = oRen.Item("pago_tipo")
        Catch
            Me.Pago_tipo = ""
        End Try
        Try
            Me.Entradapropia = oRen.Item("entradapropia")
        Catch
            Me.Entradapropia = ""
        End Try
        Try
            Me.Cantprov = oRen.Item("cantprov")
        Catch
            Me.Cantprov = ""
        End Try
    End Sub

End Class
