﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class XtraRep_OC_BASE
    Public Empresa As String
    Public Direccion As String
    Public LugExpedicion As String
    Public FacCP As String
    Public Fecha As String
    Public Importe As String
    Public Letra As String
    Public RutaXml As String
    Public Logotipo As String
    Private Sub XtraRep_Pedidos_BeforePrint(sender As Object, e As PrintEventArgs) Handles Me.BeforePrint
        Try
            XrPic_Logo.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New System.Drawing.Bitmap(Logotipo))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub lbl_Empresa_BeforePrint(sender As Object, e As PrintEventArgs) Handles lbl_EmpresaDireccion.BeforePrint
        lbl_EmpresaDireccion.Text = Direccion.Replace("|", vbNewLine)
    End Sub
    Private Sub lbl_Empresa_BeforePrint_1(sender As Object, e As PrintEventArgs) Handles lbl_Empresa.BeforePrint
        lbl_Empresa.Text = Empresa
    End Sub

    Private Sub XrLabel25_BeforePrint(sender As Object, e As PrintEventArgs)
        ''XrLabel25.Text = XrLabel25.Text.Replace("#EMPRESA#", Empresa).Replace("#LUGAR#", LugExpedicion).Replace("#FECHA#", Fecha).Replace("#IMPORTE#", Importe).Replace("#LETRA#", Letra)
    End Sub

    Private Sub XrLabel49_BeforePrint(sender As Object, e As PrintEventArgs)
        XrLabel49.Text = FacCP
    End Sub

    Private Sub XrPic_Logo_BeforePrint(sender As Object, e As PrintEventArgs) Handles XrPic_Logo.BeforePrint
        Try
            XrPic_Logo.ImageSource = New DevExpress.XtraPrinting.Drawing.ImageSource(New System.Drawing.Bitmap(Logotipo))
        Catch ex As Exception

        End Try

    End Sub
End Class